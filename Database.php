<?php
namespace Lgs;
class Database{
	/**
	  * @type object|null
	**/
	public function databaseConnect(){
		try{
			#MySQL connection with PDO_MYSQL
			$dbc = new \PDO("mysql:host=localhost;dbname=leicestg_leicestergrammar", 'leicestg_admin', '1mc83tw9');
			$dbc->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
			return $dbc;
		}
		catch(\PDOException $e){
			file_put_contents('PDOErrors.txt', $e->getMessage(), FILE_APPEND);
		}
	}
}
?>
