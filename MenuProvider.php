<?php

namespace Lgs;
use Lgs\Entity\Menu;

class MenuProvider
{
	private $html = "";
	private $current_menu;
	private $flat_menu = array();

	public function generateMenu(Menu $menu)
	{
		$this->current_menu = $this->findCurrentMenuItem($menu);

		$this->html = "<ul data-breakpoint=\"960\">";
		$this->recursive($menu->getChildren());
		$this->html .= "</ul>";
		return $this->html;
	}

	private function recursive(array $children){

		foreach($children as $child){
			if($child === $this->current_menu){
				$this->html .= "<li class=\"active-menu\"><a href=\"{$child->getUrl()}\">{$child->getName()}</a>";
			}
			else{
				$this->html .= "<li><a href=\"{$child->getUrl()}\">{$child->getName()}</a>";
			}
			

			if($child->getChildren()){
				$this->html .= "<ul>";
				$this->recursive($child->getChildren());
				$this->html .= "</ul>";
			}

			$this->html .= "</li>";
		}
	}

	private function flattenMenu(array $children)
	{
		foreach($children as $child){
			$this->flat_menu[] = $child;
			if($child->getChildren()){
				$this->flattenMenu($child->getChildren());
			}
		}
	}

	private function intersect($child_parts, $url_parts)
    {
        $count = count($url_parts);
        //echo $count;
        $intersects = 0;
        for($i = 0; $i < $count; $i++){
            if(array_key_exists($i, $child_parts)){

                if($child_parts[$i] === $url_parts[$i]){
                    //echo $child_parts[$i];
                    $intersects++;
                }
                else{
                    break;
                }
            }
        }
        return $intersects;
    }

	public function findCurrentMenuItem($menu)
    {
    	$url = ltrim($_SERVER['REQUEST_URI'], "/");
    	

        //flatten menu to array
        $this->flattenMenu($menu->getChildren());
        $children = $this->flat_menu;

        //get exact match.
        foreach($children as $child){
            if($child->getUrl() == $url){
                return $child; 
                //echo "Exact Match"; //break; //break out of loop once exact match is found.
            }
        }

        //If no exact match found
        $url_parts = explode("/", $url);
		array_shift($url_parts);
        $search = array();
        foreach($children as $child){
            $child_parts = explode("/", ltrim($child->getUrl(), "/"));
            array_shift($child_parts);
            $search[] = array(
                'child' => $child, 
                'intersects' => $this->intersect($child_parts, $url_parts),
                'total_parts' => count($child_parts)
                );
            //echo $child->getUrl(). " Intersects: " .$this->intersectAction($child_parts, $url_parts). " Total parts: ".count($child_parts)."<br>"; 
        }

        //get max intersect values.
        $max_intersect = 0;
        foreach( $search as $k => $v ){
            $max_intersect = max(array($max_intersect, $v['intersects']));
        }

        //If a match was found:
        if($max_intersect > 0){
            $possibilities = array();
            foreach($search as $s){
                if($s['intersects'] == $max_intersect){
                   //echo $s['child']->getUrl()."<br>";
                   $possibilities[] = $s;
                }                
            }
            //echo "<br>possibilities: " . count($possibilities)."<br>";
            //If there is more than one possiblity, find child with the smallest  url parts
            $min_parts = array();
            if(count($possibilities) > 0){
                foreach($possibilities as $p){
                    $min_parts[] = $p['total_parts'];
                }
                $min_parts = min($min_parts);
                foreach($possibilities as $p){
                    if($p['total_parts'] === $min_parts){
                        echo $p['child']->getName();
                        return $p['child'];
                    }
                }
            }
            else{
                //echo "Only one possibilitiy";
                return $possibilities[0]['child'];
            }
        }
        else{
            //create a temporary menu using an array if no matches found in main menu
            $temp_menu = array();
            foreach($url_parts as $part){
                $temp_menu[] = array("name" => ucfirst($part));
            }
            return $temp_menu;
        }
    }

    public function getCurrentMenu()
    {
    	return $this->current_menu;
    }

}