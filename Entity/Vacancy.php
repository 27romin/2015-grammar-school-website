<?php

namespace Lgs\Entity;

class Vacancy
{
	private $id;
	private $title;
	private $details;
	private $type;
	private $readable_type;
	private $file;
	private $interview_date;
	private $closing_date;
	private $enabled;
	private $content_only;
	private $user;

	public function getId()
	{
		return $this->id;
	}

	public function setId($id)
	{
		$this->id = $id;
		return $this;
	}

	public function getTitle()
	{
		return $this->title;
	}

	public function setTitle($title)
	{
		$this->title = $title;
		return $this;
	}

	public function getDetails()
	{
		return $this->details;
	}

	public function setDetails($details)
	{
		$this->details = $details;
		return $this;
	}

	public function getType()
	{
		return $this->type;
	}

	public function setType($type)
	{
		$this->type = $type;
		return $this;
	}

	public function getReadableType()
	{
		return $this->readable_type;
	}

	public function setReadableType($readable_type)
	{
		$this->readable_type = $readable_type;
		return $this;
	}

	

	public function setClosingDate($closing_date = null)
	{
		$this->closing_date = $closing_date;
		return $this;
	}

	public function getClosingDate($format = null)
	{
		if($this->closing_date instanceof \DateTime){
			if($format){
				return $this->formatDate($this->closing_date, $format);
			}
			return $this->closing_date;
		}
		else{
			return \DateTime::createFromFormat('Y-m-d', $this->closing_date);
		}
	}

	public function setInterviewDate($interview_date)
	{
		if($interview_date == ""){
			$this->interview_date = null;
		}
		else{
			$this->interview_date = $interview_date;
		}
		return $this;
		
	}

	public function getInterviewDate($format = null)
	{
		if($this->interview_date instanceof \DateTime){
			if($format){
				return $this->formatDate($this->interview_date, $format);
			}
			return $this->interview_date;
		}
		else{
			return \DateTime::createFromFormat('Y-m-d', $this->interview_date);
		}
	}

	private function formatDate(\DateTime $datetime, $format)
	{
		return $datetime->format($format);
	}

	public function setFile($file = null)
	{
		if($file == ""){
			$this->file = null;
		}
		else{
			$this->file = $file;
		}
		return $this;
	}

	public function getFile()
	{
		return $this->file;
	}

	public function getEnabled()
	{
		return $this->enabled;
	}

	public function isEnabled()
	{
		return $this->enabled;
	}

	public function setEnabled($enabled)
	{
		$this->enabled = $enabled;
		return $this;
	}

	public function getContentOnly()
	{
		return $this->content_only;
	}

	public function isContentOnly()
	{
		return $this->content_only;
	}

	public function setContentOnly($content_only)
	{
		$this->content_only = $content_only;
		return $this;
	}

	public function setUser(User $user)
	{
		$this->user = $user;
		return $this;
	}

	public function getUser()
	{
		return $this->user;
	}

	public function isActive()
	{
		$today = new \DateTime();
		return $this->closing_date > $today && $this->enabled;
	}
}