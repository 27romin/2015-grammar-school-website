<?php
namespace Lgs\Entity;

use Lgs\Entity\Tag;
use Lgs\Database;

class TagRepository
{
	private $dbc = null;

	public function __construct(){
		$this->dbc = new Database();
		$this->dbc = $this->dbc->databaseConnect();
	}

	public function __destruct(){
		$dbc = null;
	}

	public function createTags($tags, $resource_id, $resource, $delimiter = ",")
	{
		if(is_array($tags)){
			$this->createTagsFromArray($tags, $resource_id, $resource);
		}
		elseif(is_string($tags)){
			$this->createTagsFromString($tags, $resource_id, $resource, $delimiter);
		}
	}

	public function createTagsFromArray(array $tags, $resource_id, $resource)
	{
		foreach($tags as $t){
			$tag = new Tag();
			$tag->setName($t);
			$this->insertTag($tag, $resource_id, $resource);
		}
	}

	public function createTagsFromString($tag_str, $resource_id, $resource, $delimiter = ","){
		$this->createTagsFromArray(explode($delimiter, $tag_str), $resource_id, $resource);
	}

	public function createTag(string $tag_str, $resource_id, $resource)
	{
		$tag = new Tag();
		$tag->setName($tag_str);
		$this->insertTag($tag, $resource_id, $resource);
	}

	public function insertTag(Tag $tag, $resource_id, $resource)
	{
		try{
			if($this->tagExists($tag)){
				$tag_id = $this->tagExists($tag, $resource);
			}
			else{
				$sql = $this->dbc->prepare("INSERT INTO tags (name) VALUES (?)");
				$sql->execute(array($tag->getName()));
				$tag_id = $this->dbc->lastInsertId();
			}

			if(!$this->tagExistsForResourceId($tag, $resource_id, $resource)){
				$tagging_sql = $this->dbc->prepare("INSERT INTO tagging (tag_id, resource_id, resource) VALUES (?, ?, ?)");
				$tagging_sql->execute(array($tag_id, $resource_id, $resource));
			}
			

		}
		catch(\PDOException $e){
			echo $e->getMessage();
		}
	}

	public function tagExists($tag)
	{
		$tags = $this->findAll();

		foreach($tags as $t){
			if($t->getName() === $tag->getName()){
				return $t->getId();
			}
		}
	}
	public function tagExistsForResourceId($tag, $resource_id, $resource)
	{
		$tags = $this->findAllByResourceID($resource_id, $resource);
		foreach($tags as $t){
			if($t->getName() === $tag->getName()){
				return $t->getId();
			}
		}
	}
	public function findAll()
	{
		try{
			$sql = $this->dbc->prepare("SELECT * FROM tags ORDER BY name ASC");
			$sql->execute();
			$sql->setFetchMode(\PDO::FETCH_OBJ);
			$tags = $sql->fetchAll();

			$tags_array = array();

			foreach($tags as $t){
				$tag = new Tag();
				$tag
					->setId($t->tag_id)
					->setName($t->name);
				$tags_array[] = $tag;
			}
			return $tags_array;
		}
		catch(\PDOException $e){
				echo $e->getMessage();
		}
	}
	public function findAllByResource($resource)
	{	
		try{
			$sql = $this->dbc->prepare("SELECT * FROM tags WHERE tag_id IN (SELECT tag_id FROM tagging WHERE resource = ?)  ORDER BY name ASC");
		
			$sql->execute(array($resource));
			$sql->setFetchMode(\PDO::FETCH_OBJ);
			$tags = $sql->fetchAll();

			$tags_array = array();

			foreach($tags as $t){
				$tag = new Tag();
				$tag
					->setId($t->tag_id)
					->setName($t->name);

				$tags_array[] = $tag;
			}
			return $tags_array;
		}
		catch(\PDOException $e){
				echo $e->getMessage();
		}	
	}

	public function findAllByResourceID($resource_id, $resource)
	{
		try{
			$sql = $this->dbc->prepare("SELECT * 
										FROM tags 
										WHERE tag_id 
										IN 	(SELECT tag_id
											 FROM tagging
											 WHERE resource_id = ? 
											 AND resource = ?)  
										ORDER BY name ASC");
			
		
			$sql->execute(array($resource_id, $resource));
			$sql->setFetchMode(\PDO::FETCH_OBJ);
			$tags = $sql->fetchAll();

			$tags_array = array();

			foreach($tags as $t){
				$tag = new Tag();
				$tag
					->setId($t->tag_id)
					->setName($t->name);

				$tags_array[] = $tag;
			}
			return $tags_array;
		}
		catch(\PDOException $e){
				echo $e->getMessage();
		}	
	}

	public function deleteTag($tag, $resource_id, $resource)
	{
		try{
			$sql = $this->dbc->prepare("DELETE FROM tagging WHERE tag_id=? AND resource_id=? AND resource=?");
			$sql->execute(array($tag->getId(), $resource_id, $resource));
		}
		catch(\PDOException $e){
			echo $e->getMessage();
		}
	}

	public function deleteTagsForResourceId($resource_id, $resource)
	{
		try{
			$sql = $this->dbc->prepare("DELETE FROM tagging WHERE resource_id=? AND resource=?");
			$sql->execute(array($resource_id, $resource));
		}
		catch(\PDOException $e){
			echo $e->getMessage();
		}
	}

}

?>