<?php

namespace Lgs\Entity;


use Lgs\Database;
use Lgs\Entity\Staff;
use Lgs\Entity\DepartmentRepository;
use Upload\Storage\FileSystem;
use Upload\File;
use PHPThumb\GD as Thumb;


class StaffRepository
{
	private $dbc = null;
	const IMAGE_PATH = '/lgs/images/staff/';

	private $types = array(
		'headmaster' => 'Headmaster',
		'deputyheadacademic' => 'Deputy Head (Academic)',
		'deputyheadpastoral' => 'Deputy Head (Pastoral)',
		'academic' => 'Academic Staff',
		'chaplain' => 'Chaplain',
		'schoolnurse' => 'School Nurse',
		'juniorheadteacher' => 'Junior School Headteacher',
		'juniordeputyhead' => 'Junior School Deputy Head',
		'headofInfants' => 'Head of Infants',
		'infantcoordinator' => 'Infant Co-ordinator',
		'infantstaff' => 'Infant Staff',
		'juniorstaff' => 'Junior School Staff',
		'subjectspecialiststaff' => 'Subject Specialist Staff',
		'nurserynurses' => 'Nursery Nurses and Classroom Assistants',
		'juniorsupportstaff' => 'Junior School Support Staff',
		'businessdirector' => 'Business Director',
		'externalrelations' => 'External Relations',
		'trustees' => 'Trustees',
		'founderpatrons' => 'Founder Patrons',
		'patrons' => 'Patrons',
		'chiefexec' => 'Chief Executive',
		'cheifexec' => 'Chief Executive',
		'support' => 'Support Staff',
	);

	public function __construct(){
		$this->dbc = new Database();
		$this->dbc = $this->dbc->databaseConnect();
	}

	public function __destruct(){
		$dbc = null;
	}

	public function findAll($limit = null, $order_by = "surname, initials"){
		try{
			$sql = "SELECT * FROM staff ORDER BY $order_by";
			
			if(!$limit){
				$query = $this->dbc->prepare($sql);
			}
			else{
				$query = $this->dbc->prepare($sql." LIMIT :limit");
				$query->bindValue(':limit',(int) $limit, \PDO::PARAM_INT);
			}

			$query->execute();
			$query->setFetchMode(\PDO::FETCH_OBJ);	
			$staff_raw = $query->fetchAll();

			$staff_array = array(); 
			foreach($staff_raw as $s){
				$staff = new Staff();
				$staff
					->setId($s->staff_id)
					->setTitle($s->title)
					->setinitials($s->initials)
					->setSurname($s->surname)
					->setQualifications($s->qualifications)
					->setPosts($s->posts)
					->setType($s->type)
					->setReadableType($this->types[$s->type])
					->setImage($s->image_path);

				//$department_repo = new DepartmentRepository();
				//$departments = $department_repo->findDepartmentsByStaff($staff);
				//$staff->setDepartments($departments);

				$staff_array[] = $staff;
			}
			return $staff_array;
		}
		catch(\PDOException $e){
				echo $e->getMessage();
		}	
	}

	public function findAllByType($type = null, $limit = null, $order_by = "surname, initials")
	{
		if($type == null){
			return $this->findAll();
		}

		$sql = "SELECT *
				FROM staff 
				WHERE type = :type
				ORDER BY $order_by";
		try{
			if(!$limit){
				$query = $this->dbc->prepare($sql);
				$query->bindParam(':type', $type, \PDO::PARAM_STR, 4000);
			}
			else{
				$query = $this->dbc->prepare($sql." LIMIT :limit");
				$query->bindParam(':type', $type, \PDO::PARAM_STR, 4000);
				$query->bindValue(':limit',(int) $limit, \PDO::PARAM_INT);
			}

			$query->execute();
			$query->setFetchMode(\PDO::FETCH_OBJ);	
			$staff_raw = $query->fetchAll();

			$staff_array = array(); 
			foreach($staff_raw as $s){
				$staff = new Staff();
				$staff
					->setId($s->staff_id)
					->setTitle($s->title)
					->setinitials($s->initials)
					->setSurname($s->surname)
					->setQualifications($s->qualifications)
					->setPosts($s->posts)
					->setType($s->type)
					->setReadableType($this->types[$s->type])
					->setImage($s->image_path);
				$staff_array[] = $staff;
			}
			return $staff_array;
		}
		catch(\PDOException $e){
				echo $e->getMessage();
		}	
	}

	public function findOneById($id)
	{
			$sql = $this->dbc->prepare("SELECT DISTINCT * FROM staff WHERE staff_id=?");
			$sql->execute(array($id));
			$sql->setFetchMode(\PDO::FETCH_OBJ);
			$data = $sql->fetch();

			if($data){
				$tag_repo = new TagRepository();
				$staff = new Staff();
				$staff
					->setId($data->staff_id)
					->setTitle($data->title)
					->setInitials($data->initials)
					->setSurname($data->surname)
					->setQualifications($data->qualifications)
					->setPosts($data->posts)
					->setType($data->type)
					->setReadableType($this->types[$data->type])
					->setImage($data->image_path);
				return $staff;
			}
	}

	public function createStaff(Staff $staff)
	{
		$this->insertStaff($staff);
	}
	
	private function insertStaff(Staff $staff)
	{
		//Insert record

		//if magic quotes are enabled, git rid of them!
		if(get_magic_quotes_gpc()){
			$staff->setTitle(stripslashes($staff->getTitle()));
			$staff->setInitials(stripslashes($staff->getInitials()));
			$staff->setSurname(stripslashes($staff->getSurname()));
			$staff->setQualifications(stripslashes($staff->getQualifications()));
			$staff->setPosts(stripslashes($staff->getPosts()));
		}

		//Images (field must be staff_image)
		if(!empty($_FILES['staff_image']['name'])){
			//Upload image.
			$image = $this->uploadImage();
			//Create thumbnails
			$this->createThumbs($image);
			$image = $image->getNameWithExtension();
		}
		else{
			$image = null;
		}

		$sql = $this->dbc->prepare("INSERT INTO staff (title, initials, surname, qualifications, posts, type, image_path) VALUES (?,?,?,?,?,?,?)");
		$sql->execute(array(
			rtrim($staff->getTitle(), "."), 
			$staff->getInitials(), 
			$staff->getSurname(), 
			$staff->getQualifications(), 
			$staff->getPosts(), 
			$staff->getType(),
			$image,
		));

		
	}

	public function updateStaff(Staff $staff)
	{
		//Update Record
		
		//if magic quotes are enabled, git rid of them!
		if(get_magic_quotes_gpc()){
			$staff->setTitle(stripslashes($staff->getTitle()));
			$staff->setInitials(stripslashes($staff->getInitials()));
			$staff->setSurname(stripslashes($staff->getSurname()));
			$staff->setQualifications(stripslashes($staff->getQualifications()));
			$staff->setPosts(stripslashes($staff->getPosts()));
		}

		//Images (Field must be staff_image)
		if(!empty($_FILES['staff_image']['name'])){
			//Upload image.
			$image = $this->uploadImage();
			//Create thumbnails
			$this->createThumbs($image);
			$image = $image->getNameWithExtension();

		}
		else{
			$image = $staff->getImage();
		}

		$sql = $this->dbc->prepare("UPDATE staff SET title = ?, initials = ?, surname = ?, qualifications = ?, posts = ?, type = ?, image_path = ? WHERE staff_id=?");
		$sql->execute(array(
			rtrim($staff->getTitle(), "."), 
			$staff->getInitials(), 
			$staff->getSurname(), 
			$staff->getQualifications(), 
			$staff->getPosts(), 
			$staff->getType(),
			$image,
			$staff->getId(),

		));

	}

	public function deleteStaff(Staff $staff)
	{
		try{
			$sqlStaff = $this->dbc->prepare("DELETE FROM staff WHERE staff_id=?");
			$sqlStaff->execute(array($staff->getId()));

			$sql = $this->dbc->prepare("DELETE FROM department_staff WHERE staff_id=?");
			$sql->execute(array($staff->getId()));
		}
		catch(\PDOException $e){
			echo $e->getMessage();
		}
	}

	public function uploadImage()
	{
		$storage = new FileSystem($_SERVER['DOCUMENT_ROOT'].$this::IMAGE_PATH);
		$image = new File('staff_image', $storage);
		$image->setName("staff_".uniqid());

		//save image
		try{
		    $image->upload();
		}
		catch (\Exception $e) {
			$errors = $image->getErrors();
		}
		return $image;
	}

	private function createThumbs($image){
		//$options = array('resizeUp' => true, 'jpegQuality' => 90);
		try{
			$thumb_path = $_SERVER['DOCUMENT_ROOT'].$this::IMAGE_PATH;

			try{
				$thumb = new Thumb($thumb_path.$image->getNameWithExtension());
			}
			catch(\Exception $e){
				echo $e->getMessage();
			}

		    $thumb->adaptiveResize(400, 400)->save($thumb_path.$image->getName()."_thumb_large.".$image->getExtension());
			$thumb->resize(200, 200)->save($thumb_path.$image->getName()."_thumb_medium.".$image->getExtension());
			$thumb->resize(100, 100)->save($thumb_path.$image->getName()."_thumb_small.".$image->getExtension());
			
		}
		catch (Exception $e){
		    echo $e->getMessage();
		}		
	}

	public function getTypes()
	{
		return $this->types;
	}
}
?>