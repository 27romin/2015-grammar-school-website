<?php

namespace Lgs\Entity;

use Lgs\Entity\Term;
use Lgs\Database;

class TermRepository
{
	private $dbc = null;

	public function __construct()
	{
		$this->dbc = new Database;
		$this->dbc = $this->dbc->databaseConnect();
	}

	public function __destruct()
	{
		$dbc = null;
	}

	public function findOneByName($name)
	{
		try{
			$sql = $this->dbc->prepare("SELECT * FROM terms WHERE name = ?");
			$sql->execute(array($name));

			$sql->setFetchMode(\PDO::FETCH_OBJ);
			$data = $sql->fetch();

			if($data){
				$term = new Term();
				$term
					->setId($data->term_id)
					->setName($data->name)
					->setStartDate(\DateTime::createFromFormat('Y-m-d', $data->start_date))
					->setEndDate(\DateTime::createFromFormat('Y-m-d', $data->end_date));
				return $term;
			}
			else{
				throw new Exception("No term found");
			}
		}
		catch(\PDOException $e){
			echo $e->getMessage();
		}
	}

	public function getCurrentTerm()
	{
		$today = new \DateTime();
		try{
			$sql = $this->dbc->prepare("SELECT * FROM terms WHERE CURRENT_DATE BETWEEN start_date AND end_date");
			$sql->execute();

			$sql->setFetchMode(\PDO::FETCH_OBJ);
			$data = $sql->fetch();

			if($data){
				$term = new Term();
				$term
					->setId($data->term_id)
					->setName($data->name)
					->setEndDate(\DateTime::createFromFormat('Y-m-d', $data->end_date))
					->setStartDate(\DateTime::createFromFormat('Y-m-d', $data->start_date));
				return $term;
			}
			else{
				throw new \Exception("No term found");
			}
		}
		catch(\PDOException $e){
			echo $e->getMessage();
		}
	}

	public function updateTerm(Term $term)
	{
		try{
			$sql = $this->dbc->prepare("UPDATE terms SET name = ?, start_date = ?, end_date = ? WHERE term_id=?");
			
			$sql->execute(array(
				$term->getName(),
				$term->getStartDate()->format('Y-m-d'),
				$term->getEndDate()->format('Y-m-d'),			
				$term->getId(),
			));
		}
		catch(\PDOException $e){
			echo $e->getMessage();
		}
	}

	public function getAdvent()
	{
		return $this->findOneByName("advent");
	}

	public function getLent()
	{
		return $this->findOneByName("lent");
	}

	public function getTrinity()
	{
		return $this->findOneByName("trinity");
	}
}
?>