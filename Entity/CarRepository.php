<?php

namespace Lgs\Entity;


use Lgs\Entity\Car;
use Lgs\Database;

class CarRepository
{
	private $dbc = null;
	//this runs when the class starts	
	public function __construct()
	{
		$this->dbc = new Database();
		$this->dbc = $this->dbc->databaseConnect(); //conects the database
	}

	//this is the method which runs when the programme is finished with the class
	public function __destruct(){
		$dbc = null;	//closes the database connection
	}

	public function createCar(Car $car)
	{
		try{
			$sql = $this->dbc->prepare("INSERT INTO car (make, model, registration, colour) VALUES (?,?,?,?)");
			$sql->execute(array(
				//values which are being passed to the parameters of the query
				$car->getMake(), //this must match the method!!
				$car->getModel(),
				$car->getRegistration(),
				$car->getColour(),
			));

		}
		catch(\PDOException $e){
			echo $e->getMessage();
		}
	}

	public function updateCar(Car $car)
	{

	}

	public function deleteCar(Car $car)
	{

	}

	public function findAll()
	{
	//SELECT * FROM Car;

	}

	public function findOneById($id)
	{
	//SELECT * FROM Car WHERE ID = ?
	}

	public function findAllByTag($tag)
	{

	}

	public function findAllByMake($make)
	{

	}

	public function findAllByModel($model)
	{

	}

	public function findAllByColour($colour)
	{

	}

}