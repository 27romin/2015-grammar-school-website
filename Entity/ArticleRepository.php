<?php

namespace Lgs\Entity;


use Lgs\Database;
use Lgs\Entity\Article;
use Upload\Storage\FileSystem;
use Upload\File;
use PHPThumb\GD as Thumb;
use Lgs\Timer;

class ArticleRepository
{
	private $dbc = null;
	const IMAGE_PATH = '/lgs/images/news/';

	public function __construct(){
		$this->dbc = new Database();
		$this->dbc = $this->dbc->databaseConnect();
	}

	public function __destruct(){
		$dbc = null;
	}

	public function findAll($limit = null){
		try{
			$sql = "SELECT * FROM news ORDER BY date_added DESC, title";
			
			if(!$limit){
				$query = $this->dbc->prepare($sql);
			}
			else{
				$query = $this->dbc->prepare($sql." LIMIT :limit");
				$query->bindValue(':limit',(int) $limit, \PDO::PARAM_INT);
			}

			$query->execute();
			$query ->setFetchMode(\PDO::FETCH_OBJ);	
			$articles = $query->fetchAll();

			$tag_repo = new TagRepository();

			$articles_array = array(); 
			foreach($articles as $a){
				$article = new Article();
				$article
					->setId($a->news_id) 
					->setTitle($a->title) 
					->setDate(\DateTime::createFromFormat('Y-m-d H:i:s', $a->date_added))
					->setContent($a->content)
					->setImage($a->image_path)
					->setTags($tag_repo->findAllByResourceId($a->news_id, "news"));
				$articles_array[] = $article;
			}
			return $articles_array;
		}
		catch(\PDOException $e){
				echo $e->getMessage();
		}	
	}

	private function findAllByTag($tag, $limit = null)
	{
		if(is_string($tag)){
			$tag = new Tag($tag);
		}
		$sql = "SELECT *
				FROM news 
				WHERE news_id IN (
					SELECT resource_id 
					FROM tagging 
					WHERE resource = 'news' 
					AND tag_id IN (
						SELECT DISTINCT tag_id FROM tags WHERE name = :tag
					)
				)
				ORDER BY date_added DESC, title";
		try{
			if(!$limit){
				$query = $this->dbc->prepare($sql);
				$query->bindParam(':tag', $tag, \PDO::PARAM_STR, 4000);
			}
			else{
				$query = $this->dbc->prepare($sql." LIMIT :limit");
				$query->bindParam(':tag', $tag, \PDO::PARAM_STR, 4000);
				$query->bindValue(':limit',(int) $limit, \PDO::PARAM_INT);
			}

			$query->execute();
			$query->setFetchMode(\PDO::FETCH_OBJ);	
			$articles = $query->fetchAll();

			$tag_repo = new TagRepository();

			$articles_array = array();
			foreach($articles as $a){
				$article = new Article();
				$article
					->setId($a->news_id) 
					->setTitle($a->title) 
					->setDate(\DateTime::createFromFormat('Y-m-d H:i:s', $a->date_added))
					->setContent($a->content)
					->setImage($a->image_path)
					->setTags($tag_repo->findAllByResourceId($a->news_id, "news"));
				$articles_array[] = $article;
			}
			return $articles_array;
		}
		catch(\PDOException $e){
				echo $e->getMessage();
		}	
	}
	private function getArticleIdsForTags(array $tags)
	{
		$id_list = array();
		
		foreach($tags as $tag){
			$articles = $this->findAllByTag($tag);

			$ids = array();
			foreach($articles as $article){
				$ids[] = $article->getId();
			}
			$id_list[] = $ids;

		}
		if(is_array($tags) && count($tags) > 1){
			$ids = call_user_func_array('array_intersect', $id_list);
			return $ids;
		}
		else{
			return $id_list[0];
		}

	}
	public function findAllByTags(array $tags, $limit = null, $thumbs_only = false)
	{
		if($tags[0] == null){
			return $this->findAll($limit);
		}
		$ids = $this->getArticleIdsForTags($tags);

		$id_params = array();
		$count  = 1;
		foreach($ids as $id){
			$id_params[':id'.$count] = $id;
			$count++;
		}

		$id_query = implode(",", array_keys($id_params));

		if($ids){
			$sql = "SELECT *
					FROM news 
					WHERE news_id IN ($id_query)";

			if($thumbs_only){
				$sql .= "AND image_path IS NOT NULL AND image_path <> '' ";
			}
			$sql .=	" ORDER BY date_added DESC, title";
			
			try{
				if(!$limit){
					$query = $this->dbc->prepare($sql);
				}
				else{
					$query = $this->dbc->prepare($sql." LIMIT :limit");
					$query->bindValue(':limit',(int) $limit, \PDO::PARAM_INT);
				}

				foreach($id_params as $k => $id){
					
					$query->bindValue($k, $id);
				}

				$query->execute();
				$query->setFetchMode(\PDO::FETCH_OBJ);	
				$articles = $query->fetchAll();

				$tag_repo = new TagRepository();

				$articles_array = array();
				foreach($articles as $a){
					$article = new Article();
					$article
						->setId($a->news_id) 
						->setTitle($a->title) 
						->setDate(\DateTime::createFromFormat('Y-m-d H:i:s', $a->date_added))
						->setContent($a->content)
						->setImage($a->image_path)
						->setTags($tag_repo->findAllByResourceId($a->news_id, "news"));
					$articles_array[] = $article;
				}
			
				return $articles_array;
			}
			catch(\PDOException $e){
					echo $e->getMessage();
			}				
		}
		else{
			return array();
		}		
	}

	public function findOneById($id)
	{
		try{
			$sql = $this->dbc->prepare("SELECT DISTINCT * FROM news WHERE news_id=?");
			$sql->execute(array($id));
			$sql->setFetchMode(\PDO::FETCH_OBJ);
			$data = $sql->fetch();

			if($data){
				$tag_repo = new TagRepository();
				$article = new Article();
				$article
					->setId($data->news_id) 
					->setTitle($data->title) 
					->setDate(\DateTime::createFromFormat('Y-m-d H:i:s', $data->date_added))
					->setContent($data->content)
					->setImage($data->image_path)
					->setTags($tag_repo->findAllByResourceId($data->news_id, "news"));
				return $article;
			}
			else{
				throw new \Exception("No Article found");
			}
		}
		catch(\PDOException $error){
			echo $error->getMessage();
		}
	}

	public function createArticle(Article $article, $tags = null)
	{
		$this->insertArticle($article, $tags);
	}
	
	private function insertArticle(Article $article, $tags = null)
	{
		//Images (field must be article_image)
		if(!empty($_FILES['article_image']['name'])){
			//Upload image.
			$image = $this->uploadImage();
			//Create thumbnails
			$this->createThumbs($image);
			$image = $image->getNameWithExtension();
		}
		elseif($article->getImage()){
			$image = $article->getImage();
		}
		else{
			$image = null;
		}

		//if magic quotes are enabled, git rid of them!
		if(get_magic_quotes_gpc()){
			$article->setContent(stripslashes($article->getContent()));
			$article->setTitle(stripslashes($article->getTitle()));
		}


		//Insert record
		try{
			$sql = $this->dbc->prepare("INSERT INTO news (title, content, image_path) VALUES (?,?,?)");
			$sql->execute(array(
				$article->getTitle(), 
				$article->getContent(),
				$image,
			));

		}
		catch(\PDOException $e){
			echo $e->getMessage();
		}

		//Insert tags
		if($tags){
			$tag_repo = new TagRepository();
			$tag_repo->createTags($tags, $this->dbc->lastInsertId(), "news");
		}
		
	}

	public function updateArticle(Article $article, $tags = null)
	{

		//Images (Field must be article_image)
		if(!empty($_FILES['article_image']['name'])){
			//Upload image.
			$image = $this->uploadImage();
			//Create thumbnails
			$this->createThumbs($image);
			$image = $image->getNameWithExtension();
		}
		else{
			$image = $article->getImage();
		}

		//if magic quotes are enabled, git rid of them!
		if(get_magic_quotes_gpc()){
			$article->setContent(stripslashes($article->getContent()));
			$article->setTitle(stripslashes($article->getTitle()));
		}

		//Update Record
		try{
			$sql = $this->dbc->prepare("UPDATE news SET title = ?, content = ?, image_path = ? WHERE news_id=?");
			$sql->execute(array(
				$article->getTitle(), 
				$article->getContent(),
				$image,
				$article->getId(),
			));
		}
		catch(\PDOException $e){
			echo $e->getMessage();
		}

		//Update Tags
		if($tags){
			$tag_repo = new TagRepository();
			$tag_repo->deleteTagsForResourceId($article->getId(), "news"); //not very efficient but meh.
			$tag_repo->createTags($tags, $article->getId(), "news");
		}
	}

	public function deleteArticle(Article $article)
	{
		try{
			$sql = $this->dbc->prepare("DELETE FROM news WHERE news_id=?");
			$sql->execute(array($article->getId()));

			if($article->getTags()){
				$sql_tags = $this->dbc->prepare("DELETE FROM tagging WHERE resource_id=? AND resource = 'news'");
				$sql_tags->execute(array($article->getId()));
			}
		}
		catch(\PDOException $e){
			echo $e->getMessage();
		}

		//delete tags
		if($article->getTags()){
			$tag_repo = new TagRepository();
			$tag_repo->deleteTagsForResourceId($article->getId(), "news");
		}
	}

	private function uploadImage()
	{
		$storage = new FileSystem($_SERVER['DOCUMENT_ROOT'].$this::IMAGE_PATH);
		$image = new File('article_image', $storage);
		$image->setName("article_".uniqid());

		//save image
		try{
		    $image->upload();
		}
		catch (\Exception $e) {
			$errors = $image->getErrors();
		}
		return $image;
	}
	private function createThumbs($image){
		//$options = array('resizeUp' => true, 'jpegQuality' => 90);
		try{
			$thumb_path = $_SERVER['DOCUMENT_ROOT'].$this::IMAGE_PATH;

			try{
				$thumb = new Thumb($thumb_path.$image->getNameWithExtension());
			}
			catch(\Exception $e){
				echo $e->getMessage();
			}

		    $thumb->adaptiveResize(400, 400)->save($thumb_path.$image->getName()."_thumb_large.".$image->getExtension());
			$thumb->resize(200, 200)->save($thumb_path.$image->getName()."_thumb_medium.".$image->getExtension());
			$thumb->resize(100, 100)->save($thumb_path.$image->getName()."_thumb_small.".$image->getExtension());
			
		}
		catch (Exception $e){
		    echo $e->getMessage();
		}		
	}
}
?>