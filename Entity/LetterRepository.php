<?php

namespace Lgs\Entity;


use Lgs\Database;
use Lgs\Entity\Letter;
use Upload\Storage\FileSystem;
use Upload\File;
use PHPThumb\GD as Thumb;
use Lgs\Timer;

class LetterRepository
{
	private $dbc = null;
	const FILE_PATH = '/lgs/documents/letters/';

	public function __construct(){
		$this->dbc = new Database();
		$this->dbc = $this->dbc->databaseConnect();
	}

	public function __destruct(){
		$dbc = null;
	}

	public function findAll($limit = null, $order_by = "date DESC, title")
	{
		try{
			$sql = "SELECT * FROM letters ORDER BY $order_by";
			
			if(!$limit){
				$query = $this->dbc->prepare($sql);
			}
			else{
				$query = $this->dbc->prepare($sql." LIMIT :limit");
				$query->bindValue(':limit',(int) $limit, \PDO::PARAM_INT);
			}

			$query->execute();
			$query ->setFetchMode(\PDO::FETCH_OBJ);	
			$letters = $query->fetchAll();

			$tag_repo = new TagRepository();

			$letters_array = array(); 
			foreach($letters as $l){
				$letter = new Letter();
				$letter
					->setId($l->letter_id) 
					->setTitle($l->title) 
					->setDate(\DateTime::createFromFormat('Y-m-d', $l->date))
					->setDetails($l->details)
					->setFile($l->file_path)
					->setTags($tag_repo->findAllByResourceId($l->letter_id, "letters"));
				$letters_array[] = $letter;
			}
			return $letters_array;
		}
		catch(\PDOException $e){
				echo $e->getMessage();
		}	
	}

	private function findAllByTag($tag, $limit = null, $order_by = "date DESC, title")
	{
		if(is_string($tag)){
			$tag = new Tag($tag);
		}
		$sql = "SELECT *
				FROM letters 
				WHERE letter_id IN (
					SELECT resource_id 
					FROM tagging 
					WHERE resource = 'letters' 
					AND tag_id IN (
						SELECT DISTINCT tag_id FROM tags WHERE name = :tag
					)
				)
				ORDER BY $order_by";
		try{
			if(!$limit){
				$query = $this->dbc->prepare($sql);
				$query->bindParam(':tag', $tag, \PDO::PARAM_STR, 4000);
			}
			else{
				$query = $this->dbc->prepare($sql." LIMIT :limit");
				$query->bindParam(':tag', $tag, \PDO::PARAM_STR, 4000);
				$query->bindValue(':limit',(int) $limit, \PDO::PARAM_INT);
			}

			$query->execute();
			$query->setFetchMode(\PDO::FETCH_OBJ);	
			$letters = $query->fetchAll();

			$tag_repo = new TagRepository();

			$letters_array = array();
			foreach($letters as $l){
				$letter = new Letter();
				$letter
					->setId($l->letter_id) 
					->setTitle($l->title) 
					->setDate(\DateTime::createFromFormat('Y-m-d', $l->date))
					->setDetails($l->details)
					->setFile($l->file_path)
					->setTags($tag_repo->findAllByResourceId($l->letter_id, "letters"));
				$letters_array[] = $letter;
			}
			return $letters_array;
		}
		catch(\PDOException $e){
				echo $e->getMessage();
		}	
	}
	private function getLetterIdsForTags(array $tags)
	{
		$id_list = array();
		
		foreach($tags as $tag){
			$letters = $this->findAllByTag($tag);

			$ids = array();
			foreach($letters as $letter){
				$ids[] = $letter->getId();
			}
			$id_list[] = $ids;

		}
		if(is_array($tags) && count($tags) > 1){
			$ids = call_user_func_array('array_intersect', $id_list);
			return $ids;
		}
		else{
			return $id_list[0];
		}

	}
	public function findAllByTags(array $tags, $limit = null, $order_by = "date DESC, title")
	{
		if($tags[0] == null){
			return $this->findAll($limit, $order_by);
		}

		$ids = $this->getLetterIdsForTags($tags);

		$id_params = array();
		$count  = 1;
		foreach($ids as $id){
			$id_params[':id'.$count] = $id;
			$count++;
		}

		$id_query = implode(",", array_keys($id_params));

		if($ids){
			$sql = "SELECT *
					FROM letters 
					WHERE letter_id IN ($id_query)
					ORDER BY $order_by";
			
			try{
				if(!$limit){
					$query = $this->dbc->prepare($sql);
				}
				else{
					$query = $this->dbc->prepare($sql." LIMIT :limit");
					$query->bindValue(':limit',(int) $limit, \PDO::PARAM_INT);
				}

				foreach($id_params as $k => $id){
					$query->bindValue($k, $id);
				}

				$query->execute();
				$query->setFetchMode(\PDO::FETCH_OBJ);	
				$letters = $query->fetchAll();

				$tag_repo = new TagRepository();

				$letters_array = array();
				foreach($letters as $l){
					$letter = new Letter();
					$letter
						->setId($l->letter_id) 
						->setTitle($l->title) 
						->setDate(\DateTime::createFromFormat('Y-m-d', $l->date))
						->setDetails($l->details)
						->setFile($l->file_path)
						->setTags($tag_repo->findAllByResourceId($l->letter_id, "letters"));
					$letters_array[] = $letter;
				}
			
				return $letters_array;
			}
			catch(\PDOException $e){
					echo $e->getMessage();
			}				
		}
		else{
			return array();
		}		
	}
	public function findAllByTagsSevenDays(array $tags, $limit = 5, $order_by = "date DESC, title")
	{
		$today = new \DateTime();
		$from = new \DateTime();
		$from = $from->add(\DateInterval::createFromDateString('-7 days'));
		$letters = $this->findAllByTags($tags, $limit, $order_by);

		//echo $from->format('d/m/Y');
		//echo $today->format('d/m/Y');
		$letters_array = array();

		foreach($letters as $letter){
			if($letter->getDate() >= $from && $letter->getDate() <= $today){
				$letters_array[] = $letter;
			}
		}
		//echo count($letters_array);
		return $letters_array;
	}
	public function findOneById($id)
	{
		try{
			$sql = $this->dbc->prepare("SELECT DISTINCT * FROM letters WHERE letter_id=?");
			$sql->execute(array($id));
			$sql->setFetchMode(\PDO::FETCH_OBJ);
			$data = $sql->fetch();

			if($data){
				$tag_repo = new TagRepository();
				$letter = new Letter();
				$letter
					->setId($data->letter_id) 
					->setTitle($data->title) 
					->setDate(\DateTime::createFromFormat('Y-m-d', $data->date))
					->setDetails($data->details)
					->setFile($data->file_path)
					->setTags($tag_repo->findAllByResourceId($data->letter_id, "letters"));
				return $letter;
			}
			else{
				throw new \Exception("No Letter found");
			}
		}
		catch(\PDOException $error){
			echo $error->getMessage();
		}
	}

	public function createLetter(Letter $letter, $tags = null)
	{
		$this->insertLetter($letter, $tags);
	}
	
	private function insertLetter(Letter $letter, $tags = null)
	{
		//Files (field must be letter_file)
		if(!empty($_FILES['letter_file']['name'])){
			//Upload file.
			$file = $this->uploadFile();
			$file = $file->getNameWithExtension();
		}
		elseif($letter->getFile())
		{
			$file = $letter->getFile();
		}
		else{
			$file = null;
		}

		//if magic quotes are enabled, git rid of them!
		if(get_magic_quotes_gpc()){
			$letter->setDetails(stripslashes($letter->getDetails()));
			$letter->setTitle(stripslashes($letter->getTitle()));
		}

		if($letter->getDetails() == ""){
			$letter->setDetails(null);
		}

		//Insert record
		$sql = $this->dbc->prepare("INSERT INTO letters (title, details, date, file_path) VALUES (?,?,?,?)");
		$sql->execute(array(
			$letter->getTitle(), 
			$letter->getDetails(),
			$letter->getDate()->format('Y-m-d'), 
			$file,
		));


		//Insert tags
		if($tags){
			$tag_repo = new TagRepository();
			$tag_repo->createTags($tags, $this->dbc->lastInsertId(), "letters");
		}
		
	}

	public function updateLetter(Letter $letter, $tags = null)
	{

		//Files (Field must be letter_file)
		if(!empty($_FILES['letter_file']['name'])){
			//Upload file.
			$file = $this->uploadFile();
			$file = $file->getNameWithExtension();

		}
		else{
			$file = $letter->getFile();
		}

		//if magic quotes are enabled, git rid of them!
		if(get_magic_quotes_gpc()){
			$letter->setDetails(stripslashes($letter->getDetails()));
			$letter->setTitle(stripslashes($letter->getTitle()));
		}
		
		if($letter->getDetails() == ""){
			$letter->setDetails(null);
		}

		//Update Record
		$sql = $this->dbc->prepare("UPDATE letters SET title = ?, details = ?, date = ?, file_path = ? WHERE letter_id=?");
		$sql->execute(array(
			$letter->getTitle(), 
			$letter->getDetails(),
			$letter->getDate()->format('Y-m-d'), 
			$file,
			$letter->getId(),
		));


		//Update Tags
		if($tags){
			$tag_repo = new TagRepository();
			$tag_repo->deleteTagsForResourceId($letter->getId(), "letters"); //not very efficient but meh.
			$tag_repo->createTags($tags, $letter->getId(), "letters");
		}
	}

	public function deleteLetter(Letter $letter)
	{
		
		$sql = $this->dbc->prepare("DELETE FROM letters WHERE letter_id=?");
		$sql->execute(array($letter->getId()));

		if($letter->getTags()){
			$sql_tags = $this->dbc->prepare("DELETE FROM tagging WHERE resource_id=? AND resource = 'letters'");
			$sql_tags->execute(array($letter->getId()));
		}
		
		//delete tags
		if($letter->getTags()){
			$tag_repo = new TagRepository();
			$tag_repo->deleteTagsForResourceId($letter->getId(), "letters");
		}
	}

	private function uploadFile()
	{
		$storage = new FileSystem($_SERVER['DOCUMENT_ROOT'].$this::FILE_PATH);
		$file = new File('letter_file', $storage);
		$file->setName("letter_".uniqid());

		//save file
		try{
		    $file->upload();
		}
		catch (\Exception $e) {
			$errors = $file->getErrors();
		}
		return $file;
	}
}

?>