<?php

namespace Lgs\Entity;

use Lgs\Database;
use Lgs\Entity\Notification;

class NotificationRepository
{
	private $dbc = null;

	public function __construct(){
		$this->dbc = new Database();
		$this->dbc = $this->dbc->databaseConnect();
	}

	public function __destruct(){
		$dbc = null;
	}

	public function getNotification()
	{
		try{
			$sql = $this->dbc->prepare("SELECT DISTINCT * FROM notification");
			$sql->execute();
			$sql->setFetchMode(\PDO::FETCH_OBJ);
			$data = $sql->fetch();

			if(count($data) == 1) {
				$notification = new Notification();
				$notification->setContent($data->content)->setUpdated($data->date_updated);

				if($data->enabled == 1){
					$notification->setEnabled(true);
				}	
				elseif($data->enabled == 0){
					$notification->setEnabled(false);
				}
				else{
					$notification->setEnabled(false);
				}

				return $notification;
			}
			else{
				throw new \Exception('No notification could be found');
			}
		}
		catch(\PDOException $error){
			echo $error->getMessage();
		}
	}

	public function updateNotification(Notification $notification)
	{

		$enabled = false;
		if($notification->getContent() == ""){
			$notification->setContent(null);
		}

		if($notification->isEnabled()){
			$enabled = 1;
		}
		else{
			$enabled = 0;
		}

		//if magic quotes are enabled, git rid of them!
		if(get_magic_quotes_gpc()){
			$notification->setContent(stripslashes($notification->getContent()));
		}

		//Update Record
		$sql = $this->dbc->prepare("UPDATE notification SET content = ?, enabled = ? WHERE notification_id = 1");
		$sql->execute(array(
			$notification->getContent(),
			$enabled,
		));
	}
}