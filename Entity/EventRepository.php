<?php

namespace Lgs\Entity;


use Lgs\Database;
use Lgs\Entity\Event;
use Upload\Storage\FileSystem;
use Upload\File;
use PHPThumb\GD as Thumb;

class EventRepository
{
	private $dbc = null;
	const IMAGE_PATH = '/lgs/images/events/';

	public function __construct(){
		$this->dbc = new Database();
		$this->dbc = $this->dbc->databaseConnect();
	}

	public function __destruct(){
		$dbc = null;
	}

	public function findAll($limit = null, $thumbs_only = false, $order_by = 'date, name'){
		try{

			$sql = "SELECT * FROM events WHERE date >= CURRENT_DATE ";
			if($thumbs_only){
				$sql .= "AND image_path IS NOT NULL AND image_path <> '' ";
			}
			$sql .= "ORDER BY $order_by";

			if(!$limit){
				$query = $this->dbc->prepare($sql);
			}
			else{
				$query = $this->dbc->prepare($sql." LIMIT :limit");
				$query->bindValue(':limit',(int) $limit, \PDO::PARAM_INT);
			}

			$query->execute();
			$query ->setFetchMode(\PDO::FETCH_OBJ);	
			$events = $query->fetchAll();

			$tag_repo = new TagRepository();

			$events_array = array(); //empty
			foreach($events as $e){
				$event = new Event();
				$event
					->setId($e->event_id) //1
					->setName($e->name) //Sports Day
					->setDate(\DateTime::createFromFormat('Y-m-d', $e->date))
					->setToDate(\DateTime::createFromFormat('Y-m-d', $e->to_date))
					->setTime(\DateTime::createFromFormat('H:i:s', $e->time))
					->setToTime(\DateTime::createFromFormat('H:i:s', $e->to_time))
					->setDetails($e->details)
					->setTicketUrl($e->ticket_url)
					->setPrice($e->price)
					->setLink($e->link) //google.com/sports
					->setLocation($e->location)
					->setImage($e->image_path)
					->setYoutube($e->youtube)
					->setTags($tag_repo->findAllByResourceId($e->event_id, "events"));
				$events_array[] = $event;
			}
			return $events_array;
		}
		catch(\PDOException $e){
				echo $e->getMessage();
		}	
	}

	public function findAllByTag($tag = null, $limit = null, $thumbs_only = false, $order_by = 'date, name')
	{
		if($tag == null){
			return $this->findAll($limit, $thumbs_only, $order_by);
		}
		if(is_string($tag)){
			$tag = new Tag($tag);
		}
		$sql = "SELECT *
				FROM events 
				WHERE date >= CURRENT_DATE
				AND event_id IN (
					SELECT resource_id 
					FROM tagging 
					WHERE resource = 'events' 
					AND tag_id IN (
						SELECT DISTINCT tag_id FROM tags WHERE name = :tag
					)
				)";

		if($thumbs_only){
			$sql .= " AND image_path IS NOT NULL AND image_path <> '' ";
		}
		$sql .= "ORDER BY $order_by";

		try{
			if(!$limit){
				$query = $this->dbc->prepare($sql);
				$query->bindParam(':tag', $tag, \PDO::PARAM_STR, 4000);
			}
			else{
				$query = $this->dbc->prepare($sql." LIMIT :limit");
				$query->bindParam(':tag', $tag, \PDO::PARAM_STR, 4000);
				$query->bindValue(':limit',(int) $limit, \PDO::PARAM_INT);
			}

			$query->execute();
			$query ->setFetchMode(\PDO::FETCH_OBJ);	
			$events = $query->fetchAll();

			$tag_repo = new TagRepository();

			$events_array = array();
			foreach($events as $e){
				$event = new Event();
				$event
					->setId($e->event_id)
					->setName($e->name)
					->setDate(\DateTime::createFromFormat('Y-m-d', $e->date))
					->setToDate(\DateTime::createFromFormat('Y-m-d', $e->to_date))
					->setTime(\DateTime::createFromFormat('H:i:s', $e->time))
					->setToTime(\DateTime::createFromFormat('H:i:s', $e->to_time))
					->setDetails($e->details)
					->setTicketUrl($e->ticket_url)
					->setPrice($e->price)
					->setLink($e->link)
					->setLocation($e->location)
					->setImage($e->image_path)
					->setYoutube($e->youtube)
					->setTags($tag_repo->findAllByResourceId($e->event_id, "events"));
				$events_array[] = $event;
			}
			return $events_array;
		}
		catch(\PDOException $e){
				echo $e->getMessage();
		}	
	}

	public function findAllByTags(array $tags, $limit = null, $thumbs_only = false, $order_by = 'date, name' )
	{
		if($tags[0] == null){
			return $this->findAll($limit, $thumbs_only, $order_by);
		}
		$ids = $this->getEventIdsForTags($tags);

		$id_params = array();
		$count  = 1;
		foreach($ids as $id){
			$id_params[':id'.$count] = $id;
			$count++;
		}

		$id_query = implode(",", array_keys($id_params));

		if($ids){
			$sql = "SELECT *
					FROM events 
					WHERE event_id IN ($id_query)";

			if($thumbs_only){
				$sql .= "AND image_path IS NOT NULL AND image_path <> '' ";
			}
			$sql .=	" ORDER BY $order_by";
			
			try{
				if(!$limit){
					$query = $this->dbc->prepare($sql);
				}
				else{
					$query = $this->dbc->prepare($sql." LIMIT :limit");
					$query->bindValue(':limit',(int) $limit, \PDO::PARAM_INT);
				}

				foreach($id_params as $k => $id){
					
					$query->bindValue($k, $id);
				}

				$query->execute();
				$query->setFetchMode(\PDO::FETCH_OBJ);	
				$events = $query->fetchAll();

				$tag_repo = new TagRepository();

				$events_array = array();
				foreach($events as $e){
					$event = new Event();
					$event
						->setId($e->event_id)
						->setName($e->name)
						->setDate(\DateTime::createFromFormat('Y-m-d', $e->date))
						->setToDate(\DateTime::createFromFormat('Y-m-d', $e->to_date))
						->setTime(\DateTime::createFromFormat('H:i:s', $e->time))
						->setToTime(\DateTime::createFromFormat('H:i:s', $e->to_time))
						->setDetails($e->details)
						->setTicketUrl($e->ticket_url)
						->setPrice($e->price)
						->setLink($e->link)
						->setLocation($e->location)
						->setImage($e->image_path)
						->setYoutube($e->youtube)
						->setTags($tag_repo->findAllByResourceId($e->event_id, "events"));
					$events_array[] = $event;
				}
				return $events_array;
			}
			catch(\PDOException $e){
					echo $e->getMessage();
			}				
		}
		else{
			return array();
		}		
	}

	private function getEventIdsForTags(array $tags, $limit = null, $thumbs_only = false, $order_by = 'date, name')
	{
		$id_list = array();
		
		foreach($tags as $tag){
			$events = $this->findAllByTag($tag, $limit, $thumbs_only, $order_by);

			$ids = array();
			foreach($events as $event){
				$ids[] = $event->getId();
			}
			$id_list[] = $ids;

		}
		if(is_array($tags) && count($tags) > 1){
			$ids = call_user_func_array('array_intersect', $id_list);
			return $ids;
		}
		else{
			return $id_list[0];
		}

	}

	public function findOneById($id)
	{
		try{
			$sql = $this->dbc->prepare("SELECT DISTINCT * FROM events WHERE event_id=?");
			$sql->execute(array($id));
			$sql->setFetchMode(\PDO::FETCH_OBJ);
			$data = $sql->fetch();

			if($data){

				$tag_repo = new TagRepository();
				$event = new Event();

				$event
					->setId($data->event_id)
					->setName($data->name)
					->setDate(\DateTime::createFromFormat('Y-m-d', $data->date))
					->setToDate(\DateTime::createFromFormat('Y-m-d', $data->to_date))
					->setTime(\DateTime::createFromFormat('H:i:s', $data->time))
					->setToTime(\DateTime::createFromFormat('H:i:s', $data->to_time))
					->setDetails($data->details)
					->setTicketUrl($data->ticket_url)
					->setPrice($data->price)
					->setLink($data->link)
					->setLocation($data->location)
					->setImage($data->image_path)
					->setYoutube($data->youtube)
					->setTags($tag_repo->findAllByResourceId($data->event_id, "events"));
				return $event;
			}
			else{
				throw new \Exception("No Event found");
			}
		}
		catch(\PDOException $error){
			echo $error->getMessage();
		}

		
	}

	public function createEvent(Event $event, $tags = null)
	{
		$this->insertEvent($event, $tags);
	}
	
	public function insertEvent(Event $event, $tags = null)
	{
		//Handle optional DateTime
		if($event->getToDate())
			$to_date = $event->getToDate()->format('Y-m-d');
		else
			$to_date = null;
		if($event->getTime())
			$time = $event->getTime()->format('H:i').':00';
		else
			$time = null;
		if($event->getToTime())
			$to_time = $event->getToTime()->format('H:i').':00';
		else
			$to_time = null;

		//if magic quotes are enabled, git rid of them!
		if(get_magic_quotes_gpc()){
			$event->setDetails(stripslashes($event->getDetails()));
			$event->setName(stripslashes($event->getName()));
		}
		//Validation...

		//Images (field must be event_image)
		if(!empty($_FILES['event_image']['name'])){
			//Upload image.
			$image = $this->uploadImage();
			//Create thumbnails
			$this->createThumbs($image);
			$image = $image->getNameWithExtension();
		}
		else{
			$image = null;
		}

		//Insert record
		
		$sql = $this->dbc->prepare("INSERT INTO events (name, date, to_date, time, to_time, details, ticket_url, price, link, location, image_path, youtube) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
		$sql->execute(array(
			$event->getName(), 
			$event->getDate()->format('Y-m-d'), 
			$to_date, 
			$time,
			$to_time,
			$event->getDetails(), 
			$event->getTicketUrl(),
			$event->getPrice(),
			$event->getLink(),
			$event->getLocation(),
			$image,
			$event->getYoutube(),
		));

		

		//Insert tags
		if($tags){
			$tag_repo = new TagRepository();
			$tag_repo->createTags($tags, $this->dbc->lastInsertId(), "events");
		}
		
	}

	public function updateEvent(Event $event, $tags = null)
	{
		//Handle optional DateTime
		if($event->getToDate())
			$to_date = $event->getToDate()->format('Y-m-d');
		else
			$to_date = null;
		if($event->getTime())
			$time = $event->getTime()->format('H:i').':00';
		else
			$time = null;
		if($event->getToTime())
			$to_time = $event->getToTime()->format('H:i').':00';
		else
			$to_time = null;

		//if magic quotes are enabled, git rid of them!
		if(get_magic_quotes_gpc()){
			$event->setDetails(stripslashes($event->getDetails()));
			$event->setName(stripslashes($event->getName()));
		}

		//Images (Field must be event_image)
		if(!empty($_FILES['event_image']['name'])){
			//Upload image.
			$image = $this->uploadImage();
			//Create thumbnails
			$this->createThumbs($image);
			$image = $image->getNameWithExtension();

		}
		else{
			$image = $event->getImage();
		}

		
		$sql = $this->dbc->prepare("UPDATE events SET name = ?, date = ?, to_date = ?, time = ?, to_time = ?, details = ?, ticket_url = ?, price = ?, link = ?, location = ?, image_path = ?, youtube = ? WHERE event_id=?");
		$sql->execute(array(
			$event->getName(),
			$event->getDate()->format('Y-m-d'), 
			$to_date,
			$time,
			$to_time,
			$event->getDetails(), 
			$event->getTicketUrl(), 
			$event->getPrice(),
			$event->getLink(),
			$event->getLocation(),
			$image,
			$event->getYoutube(),
			$event->getId(),
		));
		

		//Update Tags
		if($tags){
			$tag_repo = new TagRepository();
			$tag_repo->deleteTagsForResourceId($event->getId(), "events"); //not very efficient but meh.
			$tag_repo->createTags($tags, $event->getId(), "events");
		}
	}

	public function deleteEvent(Event $event)
	{
		try{
			$sql = $this->dbc->prepare("DELETE FROM events WHERE event_id=?");
			$sql->execute(array($event->getId()));

			if($event->getTags()){
				$sql_tags = $this->dbc->prepare("DELETE FROM tagging WHERE resource_id=? AND resource = 'events'");
				$sql_tags->execute(array($event->getId()));
			}
		}
		catch(\PDOException $e){
			echo $e->getMessage();
		}

		//delete tags
		if($event->getTags()){
			$tag_repo = new TagRepository();
			$tag_repo->deleteTagsForResourceId($event->getId(), "events");
		}
	}

	public function uploadImage()
	{
		$storage = new FileSystem($_SERVER['DOCUMENT_ROOT'].$this::IMAGE_PATH);
		$image = new File('event_image', $storage);
		$image->setName("events_".uniqid());

		//save image
		try{
		    $image->upload();
		}
		catch (\Exception $e) {
			$errors = $image->getErrors();
		}
		return $image;
	}
	private function createThumbs($image){
		//$options = array('resizeUp' => true, 'jpegQuality' => 90);
		try{
			$thumb_path = $_SERVER['DOCUMENT_ROOT'].$this::IMAGE_PATH;

			try{
				$thumb = new Thumb($thumb_path.$image->getNameWithExtension());
			}
			catch(\Exception $e){
				echo $e->getMessage();
			}

		    $thumb->adaptiveResize(400, 400)->save($thumb_path.$image->getName()."_thumb_large.".$image->getExtension());
			$thumb->resize(200, 200)->save($thumb_path.$image->getName()."_thumb_medium.".$image->getExtension());
			$thumb->resize(100, 100)->save($thumb_path.$image->getName()."_thumb_small.".$image->getExtension());
			
		}
		catch (Exception $e){
		    echo $e->getMessage();
		}		
	}
}
?>