<?php

namespace Lgs\Entity;

class Menu
{
	private $name;
	private $url;
	private $children;

	public function __construct($name, $url = null, array $children = null)
	{
		$this->name = $name;
		$this->url = $url;
		$this->children = $children;
	}

	public function setName($name)
	{
		$this->name= $name;
		return $this;
	}

	public function getName()
	{
		return $this->name;
	}

	public function setUrl($url)
	{
		$this->url = $url;
		return $this;
	}

	public function getUrl()
	{
		return $this->url;
	}

	public function setChildren(array $children)
	{
		$this->children = $children;
		return $this;
	}

	public function getChildren()
	{
		return $this->children;
	}
}