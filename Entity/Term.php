<?php

namespace Lgs\Entity;

class Term
{
	private $id;
	private $name;
	private $start_date;
	private $end_date;

	public function getId()
	{
		return $this->id;
	}

	public function setId($id)
	{
		$this->id = $id;
		return $this;
	}
	public function getName($uppercase = false)
	{	
		if($uppercase)
			return ucfirst($this->name);
		return $this->name;
	}

	public function setName($name)
	{
		$this->name = $name;
		return $this;
	}

	public function getStartDate()
	{
		if($this->start_date instanceof \DateTime){
			return $this->start_date;
		}
		else{
			return \DateTime::createFromFormat('Y-m-d', $this->start_date);
		}
	}

	public function setStartDate($start_date)
	{
		$this->start_date = $start_date;
		return $this;
	}

	public function getEndDate()
	{
		if($this->end_date instanceof \DateTime){
			return $this->end_date;
		}
		else{
			return \DateTime::createFromFormat('Y-m-d', $this->end_date);
		}
	}

	public function setEndDate($end_date)
	{
		$this->end_date = $end_date;
		return $this;
	}


}
?>