<?php

namespace Lgs\Entity;

use Lgs\Database;

class UserRepository
{
	private $dbc = null;

	public function __construct(){
		$this->dbc = new Database();
		$this->dbc = $this->dbc->databaseConnect();
	}

	public function __destruct(){
		$dbc = null;
	}

	
	
	public function checkUser(User $user)
	{
		//Hash password
		$user->setPassword($this->saltPassword($user->getPassword()));
		
		$sql = $this->dbc->prepare("SELECT DISTINCT * FROM users WHERE email = ? AND password = ?");
		$sql->execute(array($user->getEmail(), $user->getPassword()));
		$sql->setFetchMode(\PDO::FETCH_OBJ);
		$data = $sql->fetch();

		if($data){
			$user = new User();

			$user
				->setId($data->user_id)
				->setName($data->name)
				->setPassword(null)
				->setEmail($data->email)
				->setUserlevel($data->userlevel);
			
			return $user;
		}
		else{
			throw new \Exception("Incorrect login");
		}
	}

	public function createUser(User $user)
	{
		$this->insertUser($user);
	}

	private function insertUser(User $user)
	{
		//Hash password
		$user->setPassword($this->saltPassword($user->getPassword()));


		$sql = $this->dbc->prepare("INSERT INTO users (name, password, email, userlevel) VALUES (?, ?, ?, ?)");
		$sql->execute(array(
			$user->getName(),
			$user->getPassword(),
			$user->getEmail(),
			$user->getUserlevel(),
		));		
		$user = null;
	}

	public function updateUser(User $user)
	{
		//Hash password
		$existing_user = $this->findOneById($user->getId());
		if($existing_user->getPassword() != $user->getPassword()){
			$user->setPassword($this->saltPassword($user->getPassword()));
		}
		
		$sql = $this->dbc->prepare("UPDATE users SET name = ?, password = ?, email = ?, userlevel = ? WHERE user_id = ?");
		$sql->execute(array(
			$user->getName(),
			$user->getPassword(),
			$user->getEmail(),
			$user->getUserlevel(),
			$user->getId(),
		));
		$user = null;
	}

	public function deleteUser($user)
	{
		try{
			$sql = $this->dbc->prepare("DELETE FROM users WHERE user_id=?");
			$sql->execute(array($user->getId()));
		}
		catch(\PDOException $e){
			echo $e->getMessage();
		}
	}

	public function findOneById($id)
	{
		try{
			$sql = $this->dbc->prepare("SELECT DISTINCT * FROM users WHERE user_id = ?");
			$sql->execute(array($id));
			$sql->setFetchMode(\PDO::FETCH_OBJ);
			$data = $sql->fetch();

			if($data){
				$user = new User();

				$user
					->setId($data->user_id)
					->setName($data->name)
					->setPassword($data->password)
					->setEmail($data->email)
					->setUserlevel($data->userlevel);
					
				return $user;
			}
			else{
				echo "No user found";
			}
		}
		catch(\PDOException $error){
			echo $error->getMessage();
		}
	}

	public function findAll(){
		try{

			$sql = "SELECT * FROM users ORDER BY name";
			$query = $this->dbc->prepare($sql);
			$query->execute();
			$query->setFetchMode(\PDO::FETCH_OBJ);	
			$users = $query->fetchAll();


			$users_array = array(); //empty
			foreach($users as $u){
				$user = new User();
				$user
					->setId($u->user_id)
					->setName($u->name)
					->setPassword($u->password)
					->setEmail($u->email)
					->setUserlevel($u->userlevel);
				$users_array[] = $user;
			}
			return $users_array;
		}
		catch(\PDOException $e){
				echo $e->getMessage();
		}	
	}

	public function saltPassword($password)
	{
		$salt = "please dont_steal my password!";
		$salted_pass = $password . $salt;
		$hash = hash('sha256', $salted_pass);
		$password = "";

		return $hash;
	}
}