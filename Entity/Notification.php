<?php

namespace Lgs\Entity;

class Notification
{
	private $content;
	private $enabled;
	private $updated;

	public function getContent()
	{
		return $this->content;
	}

	public function setContent($content)
	{
		$this->content = $content;
		return $this;
	}

	public function getEnabled()
	{
		return $this->enabled;
	}

	public function isEnabled()
	{
		return $this->enabled;
	}

	public function setEnabled($enabled)
	{
		$this->enabled = $enabled;
		return $this;
	}

	public function setUpdated($updated = null)
	{
		$this->updated = $updated;
		return $this;
	}

	public function getUpdated($format = null)
	{
		if($this->updated instanceof \DateTime){
			if($format){
				return $this->formatDate($this->updated, $format);
			}
			return $this->updated;
		}
		else{
			return \DateTime::createFromFormat('Y-m-d', $this->updated);
		}
	}
}