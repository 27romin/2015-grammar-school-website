<?php

namespace Lgs\Entity;

use Lgs\Entity\Staff;

class Department
{
	private $id;
	private $name;
	private $slug;
	private $hod;
	private $staff;
	private $gallery_content;
	private $intro_content;
	private $lower_content;
	private $gcse_content;
	private $alevel_content;
	private $extra_curricular_content;
	private $trips_content;
	private $other_content;

	public function getId()
	{
		return $this->id;
	}

	public function setId($id)
	{
		$this->id = $id;
		return $this;
	}

	public function getName()
	{
		return $this->name;
	}

	public function setName($name)
	{
		$this->name = $name;
		$this->setSlug($name);
		return $this;
	}

	public function setSlug($slug)
	{
		$this->slug = $this->createSlug($slug);
		return $this;
	}

	public function getSlug()
	{
		return $this->slug;
	}

	public function getHod()
	{
		return $this->hod;
	}

	public function setHod(Staff $staff)
	{
		$this->hod = $staff;
		return $this;
	}

	public function getStaff()
	{
		return $this->staff;
	}

	public function setStaff(array $staff = null)
	{
		$this->staff = $staff;
		return $this;
	}

	public function getIntroContent()
	{
		return $this->intro_content;
	}

	public function setGalleryContent($gallery_content = null)
	{
		if($gallery_content == ""){
			$this->gallery_content = null;
		}
		else{
			$this->gallery_content = $gallery_content;
		}
		return $this;
	}

	public function getGalleryContent()
	{
		return $this->gallery_content;
	}

	public function setIntroContent($intro_content = null)
	{
		if($intro_content == ""){
			$this->intro_content = null;
		}
		else{
			$this->intro_content = $intro_content;
		}
		return $this;
	}

	public function getLowerContent()
	{
		return $this->lower_content;
	}

	public function setLowerContent($lower_content = null)
	{
		if($lower_content == ""){
			$this->lower_content = null;
		}
		else{
			$this->lower_content = $lower_content;
		}
		return $this;
	}

	public function getGcseContent()
	{
		return $this->gcse_content;
	}

	public function setGcseContent($gcse_content = null)
	{
		if($gcse_content == ""){
			$this->gcse_content = null;
		}
		else{
			$this->gcse_content = $gcse_content;
		}
		return $this;
	}

	public function getAlevelContent()
	{
		return $this->alevel_content;
	}
	
	public function setAlevelContent($alevel_content = null)
	{
		if($alevel_content == ""){
			$this->alevel_content = null;
		}
		else{
			$this->alevel_content = $alevel_content;
		}
		return $this;
	}

	public function getExtraCurricularContent()
	{
		return $this->extra_curricular_content;
	}

	public function setExtraCurricularContent($extra_curricular_content = null)
	{
		if($extra_curricular_content == ""){
			$this->extra_curricular_content = null;
		}
		else{
			$this->extra_curricular_content = $extra_curricular_content;
		}
		return $this;
	}
	
	public function getTripsContent()
	{
		return $this->trips_content;
	}

	public function setTripsContent($trips_content = null)
	{
		if($trips_content == ""){
			$this->trips_content = null;
		}
		else{
			$this->trips_content = $trips_content;
		}
		return $this;
	}

	public function getOtherContent()
	{
		return $this->other_content;
	}

	public function setOtherContent($other_content = null)
	{
		if($other_content == ""){
			$this->other_content = null;
		}
		else{
			$this->other_content = $other_content;
		}
		return $this;
	}

	public function createSlug($str, $delimiter = "-")
	{
		$clean = iconv('UTF-8', 'ASCII//TRANSLIT', $str);
		$clean = preg_replace("/[^a-zA-Z0-9\/_|+ -]/", '', $clean);
		$clean = strtolower(trim($clean, '-'));
		$clean = preg_replace("/[\/_|+ -]+/", $delimiter, $clean);
		return $clean;
	}

}

