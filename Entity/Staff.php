<?php

namespace Lgs\Entity;

class Staff
{
	private $id;
	private $title;
	private $initials;
	private $surname;
	private $qualifications;
	private $posts;
	private $departments;
	private $type;
	private $readable_type;
	private $image;


	public function getId()
	{
		return $this->id;
	}

	public function setId($id)
	{
		$this->id = $id;
		return $this;
	}

	public function getTitle($dots = false)
	{
		if($dots){
			if($this->title == "Professor"){
				return "Professor";
			}
			elseif($this->title == ""){
				return null;
			}
			elseif($this->title != "Professor"){
				return $this->title.".";
			}
		}
		else{
			return rtrim($this->title, ".");
		}
		
	}

	public function setTitle($title)
	{
		if($title == ""){
			$this->title = null;
		}
		else{
			$this->title = $title;
		}
		
		return $this;
	}

	public function getInitials()
	{
		return $this->initials;
	}

	public function setInitials($initials)
	{
		if($initials == ""){
			$this->initials = null;
		}
		else{
			$this->initials = $initials;
		}
		
		return $this;
	}

	public function getSurname()
	{
		return $this->surname;
	}

	public function setSurname($surname)
	{
		$this->surname = $surname;
		return $this;
	}

	public function getQualifications()
	{
		return $this->qualifications;
	}

	public function setQualifications($qualifications)
	{

		if($qualifications == ""){
			$this->qualifications = null;
		}
		else{
			$this->qualifications = $qualifications;
		}
		return $this;
	}

	public function getPosts()
	{
		return $this->posts;
	}

	public function setPosts($posts)
	{
		if($posts == ""){
			$this->posts = null;
		}
		else{
			$this->posts = $posts;
		}
		return $this;
	}

	public function getType()
	{
		return $this->type;
	}

	public function setType($type)
	{
		$this->type = $type;
		return $this;
	}

	public function getReadableType()
	{
		return $this->readable_type;
	}

	public function setReadableType($readable_type)
	{
		$this->readable_type = $readable_type;
		return $this;
	}

	public function setFile($file = null)
	{
		if($file == ""){
			$this->file = null;
		}
		else{
			$this->file = $file;
		}
		return $this;
	}

	public function getFile()
	{
		return $this->file;
	}

	public function getDepartments()
	{
		return $this->departments;
	}

	public function setDepartments(array $departments)
	{
		$this->departments = $departments;
	}

	public function setImage($image = null)
	{
		if($image == ""){
			$this->image = null;
		}
		else{
			$this->image = $image;
		}
		return $this;
	}

	public function getImage()
	{
		return $this->image;
	}

	public function getSmallThumb()
	{
		$ext = pathinfo($this->image, PATHINFO_EXTENSION);
		$path = rtrim($this->image, ".$ext");
		if(file_exists($_SERVER['DOCUMENT_ROOT']."/lgs/images/staff/{$path}_thumb_small.{$ext}")){
			return "{$path}_thumb_small.{$ext}";
		}
		else{
			return $this->image;
		}
	}

	public function getMediumThumb()
	{
		$ext = pathinfo($this->image, PATHINFO_EXTENSION);
		$path = rtrim($this->image, ".$ext");
		if(file_exists($_SERVER['DOCUMENT_ROOT']."/lgs/images/staff/{$path}_thumb_medium.{$ext}")){
			return "{$path}_thumb_medium.{$ext}";
		}
		else{
			return $this->image;
		}
	}

	public function getLargeThumb()
	{
		$ext = pathinfo($this->image, PATHINFO_EXTENSION);
		$path = rtrim($this->image, ".$ext");
		if(file_exists($_SERVER['DOCUMENT_ROOT']."/lgs/images/staff/{$path}_thumb_large.{$ext}")){
			return "{$path}_thumb_large.{$ext}";
		}
		else{
			return $this->image;
		}
	}

	
}