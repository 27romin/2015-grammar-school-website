<?php

namespace Lgs\Entity;

use Lgs\Entity\Department;
use Lgs\Entity\StaffRepository;
use Lgs\Database;

class DepartmentRepository
{
	private $dbc = null;


	public function __construct()
	{
		$this->dbc = new Database();
		$this->dbc = $this->dbc->databaseConnect();
	}

	public function __destruct(){
		$dbc = null;
	}
	private function insertDepartmentStaff(Staff $staff, Department $department)
	{
		$sql = $this->dbc->prepare("INSERT INTO department_staff (staff_id, department_id) VALUES (?,?)");
		$sql->execute(array(
			$staff->getId(),
			$department->getId(),
		));
	}
	private function findStaffByDepartment($department)
	{
		//Get staff:
		$sql = null;
		$sql = $this->dbc->prepare("SELECT * FROM department_staff WHERE department_id=?");
		$sql->execute(array($department->getId()));
		$sql->setFetchMode(\PDO::FETCH_OBJ);
		$department_staff_data = $sql->fetchAll();

		$staff = array();
		$staff_repo = new StaffRepository();
		foreach($department_staff_data as $department_staff){
			$staff[] = $staff_repo->findOneById($department_staff->staff_id);
		}

		usort($staff, function($a, $b)
		{
		    return strcmp($a->getSurname(), $b->getSurname());
		});


		return $staff;
	}
	public function findDepartmentsByStaff($staff)
	{
		//Get staff:
		$sql = null;
		$sql = $this->dbc->prepare("SELECT * FROM department_staff WHERE staff_id=?");
		$sql->execute(array($staff->getId()));
		$sql->setFetchMode(\PDO::FETCH_OBJ);
		$department_staff_data = $sql->fetchAll();

		$departments = array();
		
		foreach($department_staff_data as $department_staff){
			$departments[] = $this->findOneById($department_staff->department_id);
		}
		return $departments;
	}

	public function createDepartment(Department $department)
	{
		$this->insertDepartment($department);
	}

	private function insertDepartment(Department $department)
	{
		
		if(get_magic_quotes_gpc()){
			$department->setName(stripslashes($department->getName()));
			$department->setSlug(stripslashes($department->getSlug()));
			$department->setGalleryContent(stripslashes($department->getGalleryContent()));
			$department->setIntroContent(stripslashes($department->getIntroContent()));
			$department->setLowerContent(stripslashes($department->getLowerContent()));
			$department->setGcseContent(stripslashes($department->getGcseContent()));
			$department->setAlevelContent(stripslashes($department->getAlevelContent()));
			$department->setExtraCurricularContent(stripslashes($department->getExtraCurricularContent()));
			$department->setTripsContent(stripslashes($department->getTripsContent()));
			$department->setOtherContent(stripslashes($department->getOtherContent()));
		}

		//check if slug exists:

		//$department->setSlug();
		//$new_slug = $this->checkSlug($department->getSlug());
		//echo " slug: " . $new_slug;
		//$department->setSlug($new_slug);

		try{
			$sql = $this->dbc->prepare("INSERT INTO department (name, slug, hod, gallery_content, intro_content, lower_content, gcse_content, alevel_content, extra_curricular_content, trips_content, other_content) VALUES (?,?,?,?,?,?,?,?,?,?,?)");
			$sql->execute(array(
				$department->getName(),
				$department->getSlug(),
				$department->getHod()->getId(),
				$department->getGalleryContent(),
				$department->getIntroContent(),
				$department->getLowerContent(),
				$department->getGcseContent(),
				$department->getAlevelContent(),
				$department->getExtraCurricularContent(),
				$department->getTripsContent(),
				$department->getOtherContent(),
			));

			$department->setId($this->dbc->lastInsertId()); //set department ID as last inserted id.

		}
		catch(\PDOException $e){
			echo $e->getMessage();
		}

		if($department->getStaff()){
			
			foreach($department->getStaff() as $staff){
				$this->insertDepartmentStaff($staff, $department);
			}
		}
	}

	

	public function updateDepartment(Department $department)
	{
		if(get_magic_quotes_gpc()){
			$department->setName(stripslashes($department->getName()));
			$department->setSlug(stripslashes($department->getSlug()));
			$department->setGalleryContent(stripslashes($department->getGalleryContent()));
			$department->setIntroContent(stripslashes($department->getIntroContent()));
			$department->setLowerContent(stripslashes($department->getLowerContent()));
			$department->setGcseContent(stripslashes($department->getGcseContent()));
			$department->setAlevelContent(stripslashes($department->getAlevelContent()));
			$department->setExtraCurricularContent(stripslashes($department->getExtraCurricularContent()));
			$department->setTripsContent(stripslashes($department->getTripsContent()));
			$department->setOtherContent(stripslashes($department->getOtherContent()));
		}

		//TODO check if slug exists:

		
		try{
			$sql = $this->dbc->prepare("UPDATE department SET name = ?, slug = ?, hod = ?, gallery_content = ?, intro_content = ?, lower_content = ?, gcse_content = ?, alevel_content = ?, extra_curricular_content = ?, trips_content = ?, other_content = ? WHERE department_id = ?");
			$sql->execute(array(
				$department->getName(),
				$department->getSlug(),
				$department->getHod()->getId(),
				$department->getGalleryContent(),
				$department->getIntroContent(),
				$department->getLowerContent(),
				$department->getGcseContent(),
				$department->getAlevelContent(),
				$department->getExtraCurricularContent(),
				$department->getTripsContent(),
				$department->getOtherContent(),
				$department->getId(),
			));

		}
		catch(\PDOException $e){
			echo $e->getMessage();
		}

		$this->deleteDepartmentStaff($department);

		if($department->getStaff()){
			
			foreach($department->getStaff() as $staff){
				$this->insertDepartmentStaff($staff, $department);
			}
		}
	}

	public function deleteDepartment(Department $department)
	{
		try{
			$sql = $this->dbc->prepare("DELETE FROM department WHERE department_id=?");
			$sql->execute(array($department->getId()));

			if($department->getStaff()){
				$this->deleteDepartmentStaff($department);
			}
		}
		catch(\PDOException $e){
			echo $e->getMessage();
		}
	}

	private function deleteDepartmentStaff($department)
	{
		try{
			$sql_tags = $this->dbc->prepare("DELETE FROM department_staff WHERE department_id=?");
			$sql_tags->execute(array($department->getId()));
		}
		catch(\PDOException $e){
			echo $e->getMessage();
		}
		
	}

	public function findAll($limit = null)
	{
		try{
			$sql = "SELECT * FROM department ORDER BY name";
			
			if(!$limit){
				$query = $this->dbc->prepare($sql);
			}
			else{
				$query = $this->dbc->prepare($sql." LIMIT :limit");
				$query->bindValue(':limit',(int) $limit, \PDO::PARAM_INT);
			}

			$query->execute();
			$query ->setFetchMode(\PDO::FETCH_OBJ);	
			$departments = $query->fetchAll();


			$departments_array = array(); 
			$staff_repo = new StaffRepository();

			foreach($departments as $d){
				$department = new Department();
				$department
					->setId($d->department_id) 
					->setName($d->name) 
					->setHod($staff_repo->findOneById($d->hod))
					->setGalleryContent($d->gallery_content)
					->setIntroContent($d->intro_content)
					->setLowerContent($d->lower_content)
					->setGcseContent($d->gcse_content)
					->setAlevelContent($d->alevel_content)
					->setExtraCurricularContent($d->extra_curricular_content)
					->setTripsContent($d->trips_content)
					->setOtherContent($d->other_content);

				//get staff
				$staff = $this->findStaffByDepartment($department);
				$department->setStaff($staff);
					
				$departments_array[] = $department;
			}
			return $departments_array;
		}
		catch(\PDOException $e){
				echo $e->getMessage();
		}
	}

	public function findOneByName($name)
	{
		try{
			$sql = $this->dbc->prepare("SELECT DISTINCT * FROM department WHERE name=?");
			$sql->execute(array($name));
			$sql->setFetchMode(\PDO::FETCH_OBJ);
			$data = $sql->fetch();

			if($data){
				$staff_repo = new StaffRepository();
				$department = new Department();

				$department
					->setId($data->department_id) 
					->setName($data->name) 
					->setHod($staff_repo->findOneById($data->hod))
					->setGalleryContent($data->gallery_content)
					->setIntroContent($data->intro_content)
					->setLowerContent($data->lower_content)
					->setGcseContent($data->gcse_content)
					->setAlevelContent($data->alevel_content)
					->setExtraCurricularContent($data->extra_curricular_content)
					->setTripsContent($data->trips_content)
					->setOtherContent($data->other_content);

				//get staff
				$staff = $this->findStaffByDepartment($department);
				$department->setStaff($staff);

				return $department;
			}
			else{
				throw new \Exception("No Department found");
			}
		}
		catch(\PDOException $error){
			echo $error->getMessage();
		}
	}

	public function findOneById($id)
	{
		try{
			$sql = $this->dbc->prepare("SELECT DISTINCT * FROM department WHERE department_id=?");
			$sql->execute(array($id));
			$sql->setFetchMode(\PDO::FETCH_OBJ);
			$data = $sql->fetch();

			if($data){
				$staff_repo = new StaffRepository();
				$department = new Department();

				$department
					->setId($data->department_id) 
					->setName($data->name) 
					->setHod($staff_repo->findOneById($data->hod))
					->setGalleryContent($data->gallery_content)
					->setIntroContent($data->intro_content)
					->setLowerContent($data->lower_content)
					->setGcseContent($data->gcse_content)
					->setAlevelContent($data->alevel_content)
					->setExtraCurricularContent($data->extra_curricular_content)
					->setTripsContent($data->trips_content)
					->setOtherContent($data->other_content);

				//get staff
				$staff = $this->findStaffByDepartment($department);
				$department->setStaff($staff);

				return $department;
			}
			else{
				throw new \Exception("No Department found");
			}
		}
		catch(\PDOException $error){
			echo $error->getMessage();
		}
	}

	private function slugExists($slug)
	{
		$sql = null;
		$sql = $this->dbc->prepare("SELECT * FROM department WHERE slug = ?");
		$sql->execute(array($slug));
		$sql->setFetchMode(\PDO::FETCH_OBJ);
		if(count($sql->fetchAll()) == 0){
			//echo "false";
			return false;}
		else{
			//echo "true";
			return true;}
	}

	private function checkSlug($slug)
	{	
		//echo $slug;
		//echo "hello";
		$new_slug = "";
		if($this->slugExists($slug)){
			echo "\"$slug\" exists, ";
			$matches = array();
			if(preg_match('#(\d+)$#', $slug, $matches)){
				$slug_no_number = rtrim($slug, $matches[0]);
				$number = (int) $matches[0] ++;
				$new_slug = $slug_no_number . $number;
			}
			else{
				echo " add number: ";
				$new_slug = $slug . "1";
				echo $new_slug;
			}
			return $this->checkSlug($new_slug);
		}
		else{
			echo " now return slug ";
			var_dump($slug);
			return $slug;
		}
	}

}