<?php
// other pages can access information from this page
namespace Lgs\Entity;

//sets the class
class Car{

//properties are set to private so others cannot directly access
	private $id;
	private $model;
	private $make;
	private $registration;
	private $colour;

// allows anything outside the scope of the class to interact
	public function getId()
	{
		return $this->id;
	}

	public function setId($id)
	{
		$this->id = $id;
		return $this;
	}

	public function getModel()
	{
		return $this->model;
	}

	public function setModel($model)
	{
		$this->model = $model;
		return $this;
	}

	public function getMake()
	{
		return $this->make;
	}

	public function setMake($make)
	{
		$this->make = $make;
		return $this;
	}

	public function getRegistration()
	{
		return $this->registration;
	}

	public function setRegistration($registration)
	{
		$this->registration = $registration;
		return $this;
	}

	public function getColour()
	{
		return $this->colour;
	}

	public function setColour($colour)
	{
		$this->colour = $colour;
		return $this;
	}
}

