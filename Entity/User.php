<?php 
namespace Lgs\Entity;
class User
{
	private $id;
	private $name;
	private $password;
	private $email;
	private $userlevel;


	public function getId()
	{
		return $this->id;
	}

	public function setId($id)
	{
		$this->id = $id;
		return $this;
	}

	public function setName($name)
	{
		$this->name = stripslashes($name);
		return $this;
	}

	public function getName()
	{
		return $this->name;
	}

	public function setPassword($password)
	{
		$this->password = $password;
		return $this;
	}

	public function getPassword()
	{
		return $this->password;
	}

	public function setEmail($email)
	{
		$this->email = $email;
		return $this;
	}

	public function getEmail()
	{
		return $this->email;
	}


	public function setUserlevel($userlevel)
	{
		$this->userlevel = $userlevel;
		return $this;
	}

	public function getUserlevel()
	{
		return $this->userlevel;
	}

	public function getFirstName()
    {
        $names = explode(' ', $this->name);
        $values = array_values($names);
        $firstname = ucwords(array_shift($values));
        if($firstname == '' || !$firstname)
            return $this->username;
        else
            return $firstname;
    }
    public function getSurname()
    {
        $names = explode(' ', $this->name);
        $surname = ucwords(end($names));

        if($surname == '' || !$surname)
            return $this->username;
        else
            return $surname;
    }
}