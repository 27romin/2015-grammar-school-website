<?php

namespace Lgs\Entity;

class Article
{
	private $id;
	private $title;
	private $content;
	private $tags;
	private $date;
	private $image;


	public function getId()
	{
		return $this->id;
	}

	public function setId($id)
	{
		$this->id = $id;
		return $this;
	}

	public function getTitle()
	{
		return $this->title;
	}

	public function setTitle($title)
	{
		$this->title = $title;
		return $this;
	}

	public function getShortTitle($width = 30)
	{
		return $this->getShort($this->title, $width);
	}

	public function getContent()
	{
		return $this->content;
	}

	public function setContent($content)
	{
		$this->content = $content;
		return $this;
	}

	public function getShortContent($width = 100)
	{
		return $this->getShort($this->content, $width);
	}

	private function getShort($string, $width = 100)
	{
		$string = strip_tags($string);
		$parts = preg_split('/([\s\n\r]+)/', $string, null, PREG_SPLIT_DELIM_CAPTURE);
		$parts_count = count($parts);

		$length = 0;
		$last_part = 0;
		for (; $last_part < $parts_count; ++$last_part) {
			$length += strlen($parts[$last_part]);
			if ($length > $width) {
				break;
			}
		}

  		return implode(array_slice($parts, 0, $last_part));
	}
	public function setDate($date = null)
	{
		$this->date = $date;
		return $this;
	}

	public function getDate($format = null)
	{
		if($this->date instanceof \DateTime){
			if($format){
				return $this->formatDate($this->date, $format);
			}
			return $this->date;
		}
		else{
			return \DateTime::createFromFormat('Y-m-d', $this->date);
		}
	}

	public function setImage($image = null)
	{
		if($image == ""){
			$this->image = null;
		}
		else{
			$this->image = $image;
		}
		return $this;
	}

	public function getImage()
	{
		return $this->image;
	}

	public function getSmallThumb()
	{
		$ext = pathinfo($this->image, PATHINFO_EXTENSION);
		$path = rtrim($this->image, ".$ext");
		if(file_exists($_SERVER['DOCUMENT_ROOT']."/lgs/images/news/{$path}_thumb_small.{$ext}")){

			return "{$path}_thumb_small.{$ext}";
		}
		else{
			return $this->image;
		}
	}

	public function getMediumThumb()
	{
		$ext = pathinfo($this->image, PATHINFO_EXTENSION);
		$path = rtrim($this->image, ".$ext");

		if(file_exists($_SERVER['DOCUMENT_ROOT']."/lgs/images/news/{$path}_thumb_medium.{$ext}")){

			return "{$path}_thumb_medium.{$ext}";
		}
		else{
			return $this->image;
		}
	}

	public function getLargeThumb()
	{
		$ext = pathinfo($this->image, PATHINFO_EXTENSION);
		$path = rtrim($this->image, ".$ext");

		if(file_exists($_SERVER['DOCUMENT_ROOT']."/lgs/images/news/{$path}_thumb_large.{$ext}")){
			return "{$path}_thumb_large.{$ext}";
		}
		else{
			return $this->image;
		}
		
	}

	public function setTags(array $tags = null)
	{
		$this->tags = $tags;
		return $this;
	}

	public function getTags()
	{
		return $this->tags;
	}

	private function formatDate(\DateTime $datetime, $format)
	{
		return $datetime->format($format);
	}
}