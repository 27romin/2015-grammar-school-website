<?php
namespace Lgs\Entity;

use Lgs\Entity\Vacancy;
use Lgs\Entity\User;
use Lgs\Entity\UserRepository;
use Lgs\Login;
use Lgs\Database;
use Upload\Storage\FileSystem;
use Upload\File;

class VacancyRepository
{
	private $dbc = null;
	private $types = array(
		'academic' => 'Academic',
		'support' => 'Support',
	);
	const IMAGE_PATH = '/lgs/documents/vacancies/';

	public function __construct(){
		$this->dbc = new Database();
		$this->dbc = $this->dbc->databaseConnect();
	}

	public function __destruct(){
		$dbc = null;
	}

	public function findAll($limit = null, $force_all = false, $order_by = "closing_date"){
		try{
			$sql = "SELECT * FROM vacancies ";
			
			if(!$force_all){
				$sql .= "WHERE CURRENT_DATE < closing_date AND enabled = 1 ";
			}

			$sql .= "ORDER BY $order_by ";
			
			if(!$limit){
				$query = $this->dbc->prepare($sql);
			}
			else{
				$query = $this->dbc->prepare($sql." LIMIT :limit");
				$query->bindValue(':limit',(int) $limit, \PDO::PARAM_INT);
			}

			$query->execute();
			$query ->setFetchMode(\PDO::FETCH_OBJ);	
			$data = $query->fetchAll();

			$vacancy_array = array(); 
			foreach($data as $v){
				$vacancy = new Vacancy();
				$vacancy
					->setId($v->vacancy_id)
					->setTitle($v->title)
					->setDetails($v->details)
					->setType($v->type)
					->setReadableType($this->types[$v->type])
					->setFile($v->file)
					->setInterviewDate(\DateTime::createFromFormat('Y-m-d', $v->interview_date))
					->setClosingDate(\DateTime::createFromFormat('Y-m-d', $v->closing_date))
					->setEnabled((bool) $v->enabled)
					->setContentOnly((bool) $v->content_only);

				$vacancy_array[] = $vacancy;
			}
			return $vacancy_array;
		}
		catch(\PDOException $e){
				echo $e->getMessage();
		}	
	}

	public function findAllByType($type = null, $limit = null, $force_all = false, $order_by = "closing_date DESC")
	{
		if($type == null){
			return $this->findAll($limit, $force_all, $order_by);
		}

		try{
			$sql = "SELECT *
					FROM vacancies
					WHERE type = :type ";
				
			if(!$force_all){
				$sql .= "AND CURRENT_DATE < closing_date AND enabled = 1 ";
			}

			$sql .= "ORDER BY $order_by ";
			
			if(!$limit){
				$query = $this->dbc->prepare($sql);
				$query->bindParam(':type', $type, \PDO::PARAM_STR, 4000);
			}
			else{
				$sql .= " LIMIT :limit";
				$query->bindParam(':type', $type, \PDO::PARAM_STR, 4000);
				$query->bindValue(':limit',(int) $limit, \PDO::PARAM_INT);
				$query = $this->dbc->prepare($sql);
			}
		
			$query->execute();

			$query->setFetchMode(\PDO::FETCH_OBJ);	
			$data = $query->fetchAll();

			$vacancy_array = array(); 
			foreach($data as $v){
				$vacancy = new Vacancy();
				$vacancy
					->setId($v->vacancy_id)
					->setTitle($v->title)
					->setDetails($v->details)
					->setType($v->type)
					->setReadableType($this->types[$v->type])
					->setFile($v->file)
					->setInterviewDate(\DateTime::createFromFormat('Y-m-d', $v->interview_date))
					->setClosingDate(\DateTime::createFromFormat('Y-m-d', $v->closing_date))
					->setEnabled((bool) $v->enabled)
					->setContentOnly((bool) $v->content_only);
				$vacancy_array[] = $vacancy;
			}
			return $vacancy_array;
		}
		catch(\PDOException $e){
				echo $e->getMessage();
		}	
	}

	public function findOneById($id)
	{
		try{
			$sql = $this->dbc->prepare("SELECT DISTINCT * FROM vacancies WHERE vacancy_id=?");
			$sql->execute(array($id));
			$sql->setFetchMode(\PDO::FETCH_OBJ);
			$data = $sql->fetch();


			if($data){
				$vacancy = new Vacancy();
				$vacancy
					->setId($data->vacancy_id)
					->setTitle($data->title)
					->setDetails($data->details)
					->setType($data->type)
					->setReadableType($this->types[$data->type])
					->setFile($data->file)
					->setInterviewDate(\DateTime::createFromFormat('Y-m-d', $data->interview_date))
					->setClosingDate(\DateTime::createFromFormat('Y-m-d', $data->closing_date))
					->setEnabled((bool) $data->enabled)
					->setContentOnly((bool) $data->content_only);
				return $vacancy;
			}
			else{
				throw new \Exception("No Vacancy found");
			}
		}
		catch(\PDOException $error){
			echo $error->getMessage();
		}
	}

	public function createVacancy(Vacancy $vacancy)
	{
		$this->insertVacancy($vacancy);
	}
	
	private function insertVacancy(Vacancy $vacancy)
	{
		//Handle optional DateTime
		if($vacancy->getInterviewDate())
			$interview_date = $vacancy->getInterviewDate()->format('Y-m-d');
		else
			$interview_date = null;

		//Files (field must be vacancy_file)
		if(!empty($_FILES['vacancy_file']['name'])){
			//Upload file.
			$file = $this->uploadFile();
			$file = $file->getNameWithExtension();
		}
		else{
			$file = null;
		}

		//Get logged in user
		$login = new Login();
		$current_user = $login->getCurrentUser();

		//if magic quotes are enabled, git rid of them!
		if(get_magic_quotes_gpc()){
			$vacancy->setTitle(stripslashes($vacancy->getTitle()));
			$vacancy->setDetails(stripslashes($vacancy->getDetails()));
		}
	
		//Insert record
		$sql = $this->dbc->prepare("INSERT INTO vacancies (title, details, closing_date, type, file, interview_date, enabled, content_only) VALUES (?,?,?,?,?,?,?,?)");
		$sql->execute(array(
			$vacancy->getTitle(),
			$vacancy->getDetails(),
			$vacancy->getClosingDate()->format('Y-m-d'),
			$vacancy->getType(),
			$file,
			$interview_date,
			(int) $vacancy->getEnabled(),
			(int) $vacancy->getContentOnly(),
		));

		
	}

	public function updateVacancy(Vacancy $vacancy)
	{	
		//Handle optional DateTime
		if($vacancy->getInterviewDate())
			$interview_date = $vacancy->getInterviewDate()->format('Y-m-d');
		else
			$interview_date = null;

		//Files (field must be vacancy_file)
		if(!empty($_FILES['vacancy_file']['name'])){
			//Upload file.
			$file = $this->uploadFile();
			$file = $file->getNameWithExtension();
		}
		else{
			$file = $vacancy->getFile();
		}

		//if magic quotes are enabled, git rid of them!
		if(get_magic_quotes_gpc()){
			$vacancy->setTitle(stripslashes($vacancy->getTitle()));
			$vacancy->setDetails(stripslashes($vacancy->getDetails()));
		}

		//Get logged in user
		$login = new Login();
		$current_user = $login->getCurrentUser();

		//Update Record
		$sql = $this->dbc->prepare("UPDATE vacancies SET title = ?, details = ?, closing_date = ?, type = ?, file = ?, interview_date = ?, enabled = ?, content_only = ? WHERE vacancy_id=?");
		$sql->execute(array(
			$vacancy->getTitle(),
			$vacancy->getDetails(),
			$vacancy->getClosingDate()->format('Y-m-d'),
			$vacancy->getType(),
			$file,
			$interview_date,
			(int) $vacancy->getEnabled(),
			(int) $vacancy->getContentOnly(),
			$vacancy->getId(),
		));

	}

	public function deleteVacancy(Vacancy $vacancy)
	{
		try{
			$sql = $this->dbc->prepare("DELETE FROM vacancies WHERE vacancy_id=?");
			$sql->execute(array($vacancy->getId()));
		}
		catch(\PDOException $e){
			echo $e->getMessage();
		}
	}
	public function getTypes()
	{
		return $this->types;
	}

	public function uploadFile()
	{
		$storage = new FileSystem($_SERVER['DOCUMENT_ROOT'].$this::IMAGE_PATH);
		$file = new File('vacancy_file', $storage);
		$file->setName("vacancy_".uniqid());

		//save file
		try{
		    $file->upload();
		}
		catch (\Exception $e) {
			$errors = $file->getErrors();
		}
		return $file;
	}
}