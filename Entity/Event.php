<?php

namespace Lgs\Entity;

class Event
{
	private $id;
	private $name;
	private $date;
	private $to_date;
	private $time;
	private $to_time;
	private $details;
	private $ticket_url;
	private $price;
	private $link;
	private $location;
	private $image;
	private $youtube;
	private $tags;
	private $date_added;


	public function setId($id)
	{
		$this->id = $id;
		return $this;
	}

	public function getId()
	{
		return $this->id;
	}

	public function setName($name)
	{
		$this->name = stripslashes($name);
		return $this;
	}

	public function getName()
	{
		return $this->name;
	}

	private function formatDate(\DateTime $datetime, $format)
	{
		return $datetime->format($format);
	}

	public function setDate($date)
	{
		$this->date = $date;
		return $this;
	}

	public function getDate($format = null)
	{
		if($this->date instanceof \DateTime){
			if($format){
				return $this->formatDate($this->date, $format);
			}
			return $this->date;
		}
		else{
			return \DateTime::createFromFormat('Y-m-d', $this->date);
		}
	}

	public function setToDate($to_date = null)
	{
		if($to_date == ""){
			$this->to_date = null;
		}
		else{
			$this->to_date = $to_date;
		}
		return $this;
	}

	public function getToDate($format = null)
	{
		if($this->to_date instanceof \DateTime){
			if($format){
				return $this->formatDate($this->to_date, $format);
			}
			return $this->to_date;
		}
		else{
			return \DateTime::createFromFormat('Y-m-d', $this->to_date);
		}
	}

	public function setTime($time = null)
	{
		if($time == ""){
			$this->time = null;
		}
		else{	
			$this->time = $time;
		}
		return $this;
	}

	public function getTime($format = null)
	{
		if($this->time instanceof \DateTime){
			if($format){
				return $this->formatDate($this->time, $format);
			}
			return $this->time;
		}
		else{
			return \DateTime::createFromFormat('H:i', $this->time);
		}
	}

	public function setToTime($to_time = null)
	{
		if($to_time == ""){
			$this->to_time = null;
		}
		else{
			$this->to_time = $to_time;
		}
		return $this;
	}

	public function getToTime($format = null)
	{
		if($this->to_time instanceof \DateTime){
			if($format){
				return $this->formatDate($this->to_time, $format);
			}
			return $this->to_time;
		}
		else{
			return \DateTime::createFromFormat('H:i', $this->to_time);
		}
	}

	public function setDetails($details = null)
	{
		if($details == ""){
			$this->details = null;
		}
		else{
			$this->details = $details;
		}
		return $this;
	}

	public function getDetails()
	{
		return $this->details;
	}

	public function getShortDetails($width = 100)
	{
		$string = strip_tags($this->details);
		$parts = preg_split('/([\s\n\r]+)/', $string, null, PREG_SPLIT_DELIM_CAPTURE);
		$parts_count = count($parts);

		$length = 0;
		$last_part = 0;
		for (; $last_part < $parts_count; ++$last_part) {
			$length += strlen($parts[$last_part]);
			if ($length > $width) {
				break;
			}
		}

  		return implode(array_slice($parts, 0, $last_part));
	}

	public function setTicketUrl($ticket_url = null)
	{
		if($ticket_url == ""){
			$this->ticket_url = null;
		}
		else{
			$this->ticket_url = $ticket_url;
		}
		return $this;
	}

	public function getTicketUrl()
	{
		return $this->ticket_url;
	}

	public function setPrice($price = null)
	{	
		if($price == 00.00 || $price == ""){
			$this->price = null;
		}
		else{
			$this->price = $price;
		}
		return $this;
	}

	public function getPrice()
	{
		return $this->price;
	}

	public function setLink($link = null)
	{
		if($link == ""){
			$this->link = null;
		}
		else{
			$this->link = $link;
		}
		return $this;
	}

	public function getLink()
	{
		return $this->link;
	}

	public function setLocation($location = null)
	{
		if($location == ""){
			$this->location = null;
		}
		else{
			$this->location = stripslashes($location);
		}
		return $this;
	}

	public function getLocation()
	{
		return $this->location;
	}
	
	public function setImage($image = null)
	{
		if($image == ""){
			$this->image = null;
		}
		else{
			$this->image = $image;
		}
		return $this;
	}

	public function getImage()
	{
		return $this->image;
	}

	public function getSmallThumb()
	{
		$ext = pathinfo($this->image, PATHINFO_EXTENSION);
		$path = rtrim($this->image, ".$ext");
		if(file_exists($_SERVER['DOCUMENT_ROOT']."/lgs/images/events/{$path}_thumb_small.{$ext}")){
			return "{$path}_thumb_small.{$ext}";
		}
		else{
			return $this->image;
		}
	}

	public function getMediumThumb()
	{
		$ext = pathinfo($this->image, PATHINFO_EXTENSION);
		$path = rtrim($this->image, ".$ext");
		if(file_exists($_SERVER['DOCUMENT_ROOT']."/lgs/images/events/{$path}_thumb_medium.{$ext}")){
			return "{$path}_thumb_medium.{$ext}";
		}
		else{
			return $this->image;
		}
	}

	public function getLargeThumb()
	{
		$ext = pathinfo($this->image, PATHINFO_EXTENSION);
		$path = rtrim($this->image, ".$ext");
		if(file_exists($_SERVER['DOCUMENT_ROOT']."/lgs/images/events/{$path}_thumb_large.{$ext}")){
			return "{$path}_thumb_large.{$ext}";
		}
		else{
			return $this->image;
		}
	}

	public function setYoutube($youtube = null)
	{
		if($youtube == ""){
			$this->youtube = null;
		}
		else{
			$this->youtube = $youtube;
		}
		return $this;
	}
	public function getYoutube()
	{
		return $this->youtube;
	}
	public function setTags(array $tags = null)
	{
		$this->tags = $tags;
		return $this;
	}

	public function getTags()
	{
		return $this->tags;
	}

	
}
?>