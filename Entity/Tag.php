<?php
namespace Lgs\Entity;

class Tag
{
	private $id;
	private $name;

	public function __construct($name = null)
	{
		if($name){
			$this->name = $name;
		}
	}
	public function setId($id)
	{
		$this->id = $id;
		return $this;
	}

	public function getId()
	{
		return $this->id;
	}

	public function setName($name)
	{
		$this->name = $name;
		return $this;
	}

	public function getName()
	{
		return $this->name;
	}

	public function __toString()
	{
		return $this->name;
	}
}
?>