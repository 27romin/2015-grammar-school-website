<?php

namespace Lgs\Entity;

class Letter
{
	private $id;
	private $title;
	private $details;
	private $tags;
	private $date;
	private $file;


	public function getId()
	{
		return $this->id;
	}

	public function setId($id)
	{
		$this->id = $id;
		return $this;
	}

	public function getTitle()
	{
		return $this->title;
	}

	public function setTitle($title)
	{
		$this->title = $title;
		return $this;
	}

	public function getShortTitle($width = 30)
	{
		return $this->getShort($this->title, $width);
	}

	public function getDetails()
	{
		return $this->details;
	}

	public function setDetails($details)
	{
		$this->details = $details;
		return $this;
	}

	public function getShortDetails($width = 100)
	{
		return $this->getShort($this->details, $width);
	}

	private function getShort($string, $width = 100)
	{
		$string = strip_tags($string);
		$parts = preg_split('/([\s\n\r]+)/', $string, null, PREG_SPLIT_DELIM_CAPTURE);
		$parts_count = count($parts);

		$length = 0;
		$last_part = 0;
		for (; $last_part < $parts_count; ++$last_part) {
			$length += strlen($parts[$last_part]);
			if ($length > $width) {
				break;
			}
		}

  		return implode(array_slice($parts, 0, $last_part));
	}
	public function setDate($date)
	{
		$this->date = $date;
		return $this;
	}

	public function getDate($format = null)
	{
		if($this->date instanceof \DateTime){
			if($format){
				return $this->formatDate($this->date, $format);
			}
			return $this->date;
		}
		else{
			return \DateTime::createFromFormat('Y-m-d', $this->date);
		}
	}

	public function setFile($file = null)
	{
		if($file == ""){
			$this->file = null;
		}
		else{
			$this->file = $file;
		}
		return $this;
	}

	public function getFile()
	{
		return $this->file;
	}

	public function setTags(array $tags = null)
	{
		$this->tags = $tags;
		return $this;
	}

	public function getTags()
	{
		return $this->tags;
	}
	
	private function formatDate(\DateTime $datetime, $format)
	{
		return $datetime->format($format);
	}
}