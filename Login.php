<?php

namespace Lgs;

use Lgs\Entity\User;
use Lgs\Entity\UserRepository;

class Login
{
	public function __construct()
	{
		if(!isset($_SESSION)) { 
			session_start(); 
		} 
	}

	public function getCurrentUser()
	{
		$user_repo = new UserRepository();
		$user = $user_repo->findOneById($_SESSION['userid']);

		return $user;
	}

	public function login(User $user)
	{
		$user_repo = new UserRepository();

		$user = $user_repo->checkUser($user);
		if($user){
			$_SESSION['userid'] = $user->getId();
		}
	}

	public function logout()
	{
		unset($_SESSION["userid"]);
		header("Location: /admin/login");
	}

	public function isLoggedIn()
	{	
		if(isset($_SESSION['userid'])){
			return true;
		}
		else{
			return false;
		}
	}
}