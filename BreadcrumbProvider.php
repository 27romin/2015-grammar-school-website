<?php

namespace Lgs;
use Lgs\Entity\Menu;

class BreadcrumbProvider
{

	private $menu;
	private $current_menu;

	public function __construct($menu, $current_menu)
	{
		$this->menu = $menu;
		$this->current_menu = $current_menu;
	}

	function findParents($child, $stack) {
		$return = array();
	    foreach ($stack as $child) {
	        if (is_array($child->getChildren())) {
	            // If the current element of the array is an array, recurse it 
	            // and capture the return stack
	            $stack = $this->findParents($child, $child->getChildren());
	            
	            // If the return stack is an array, add it to the return
	            if (is_array($stack) && !empty($stack)) {
	                $return[] = $child;
	            }
	        } 
	        else {
	            // Since we are not on an array, compare directly
	            if ($child->getChildren() == $child) {
	                // And if we match, stack it and return it
	                $return[] = $child;
	            }
	        }
	    }
	    
	    // Return the stack
	    return empty($return) ? false: $return;
	}
	public function generateBreadcrumbs(){
		//current item

        
        //homepage url
        $homepage_url = "#";
        $site = "Site";

        $breadcrumbs = array();
            $breadcrumbs[] = array(
                    'url' => $homepage_url,
                    'name' => $site
                    );

        if($this->current_menu instanceof Menu){
        	$path = $this->findParents($this->current_menu, $this->menu);
            //array_shift($path);
            var_dump($path);      
            foreach($path as $node){
                $url = $homepage_url.$node->getUrl();
                $name = $node->getName();

                $breadcrumbs[] = array(
                    'url' => $url,
                    'name' => $name
                    );
            }
            return $breadcrumbs;

        }
        else{
            $path_str = "<a href=\"$homepage_url\">$site</a>  &gt; ";
            foreach($this->current_menu as $item){
                $breadcrumbs[] = array(
                    'url' => null,
                    'name' => $name = $item['name']
                    );
            }
            return $breadcrumbs;
        }
    }
}